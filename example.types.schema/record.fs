namespace Example.Types.Schema

type Record = {
    Name : string
    Properties : Property[]
    Implements : string
}
     