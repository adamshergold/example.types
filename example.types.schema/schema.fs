namespace Example.Types.Schema

open Microsoft.Extensions.Logging

open FParsec

open Example.Types.Schema.Extensions

type SchemaOptions = {
    Logger : ILogger option
    AdditionalScalars : string[]
}
with
    static member Default = {
        Logger = None
        AdditionalScalars = [| "Serialisable"; "LocalDateTime"; "LocalDate"; "ZonedDateTime" |]
    }
    
type Schema( options: SchemaOptions ) as this =
    
    let items =
        new System.Collections.Generic.Dictionary<string,Definition>()
    
    let onAddedOrUpdated =
        new Event<AddedOrUpdated>()
        
    let onRemoved =
        new Event<Removed>()
        
    do
        this.Setup()
        
    static member Make() =
        new Schema( SchemaOptions.Default ) :> ISchema
                       
    static member Make( options ) =
        new Schema( options ) :> ISchema
        
    member this.Setup () =
        seq {
            yield "Int"
            yield "Float"
            yield "String"
            yield "Boolean"
            yield "Double"
            yield! options.AdditionalScalars
        }
        |> Seq.iter ( fun scalar ->
            items.Add( scalar, Definition.Make( sprintf "scalar %s" scalar, Element.Scalar( Scalar.Make( scalar, true) ) ) ) )
        
    member this.Dispose () =
        items.Clear()
        
    member this.Count
        with get () = items.Count
        
    member this.Clear() =
        lock items <| fun _ ->
            items.Clear()
            this.Setup()            
            
    member this.Items
        with get () =
            items.Values |> Seq.cast
        
    member this.TryLookup name =
        lock this <| fun _ ->
            match items.TryGetValue name with
            | true, definition -> Some definition
            | false, _ -> None
        
    member this.AddOrUpdate (definition:Definition) =
        lock items <| fun _ ->

            if items.ContainsKey definition.Element.Name then
                items.Remove( definition.Element.Name ) |> ignore
               
            items.Add( definition.Element.Name, definition )     
            
        onAddedOrUpdated.Trigger <| AddedOrUpdated.Make( definition.Element.Name, definition.Source ) 
        
    member this.Remove (name:string) =
        
        let removed =
            lock items <| fun _ -> items.Remove( name ) 

        if removed then 
            onRemoved.Trigger <| Removed.Make( name )
            
        removed            
        
        
    member this.IsBuiltIn (name:string) =
        match this.TryLookup name with
        | Some definition ->
            match definition.Element with
            | Element.Scalar(s) -> s.IsBuiltIn
            | _ ->false
        | None ->
            failwithf "Unable to find schema element with name '%s'" name
            
    member this.IsEnum (name:string) =
        match this.TryLookup name with
        | Some definition ->
            match definition.Element with
            | Element.Enum(_) ->true
            | _ ->false
        | None ->
            false

    member this.IsScalar (name:string) =
        match this.TryLookup name with
        | Some definition ->
            match definition.Element with
            | Element.Scalar(_) ->true
            | _ ->false
        | None ->
            false
          
    member this.IsRecord (name:string) =
        match this.TryLookup name with
        | Some definition ->
            match definition.Element with
            | Element.Record(_) ->true
            | _ ->false
        | None ->
            false

    member this.IsInterface (name:string) =
        match this.TryLookup name with
        | Some definition ->
            match definition.Element with
            | Element.Interface(_) ->true
            | _ ->false
        | None ->
            false
    
    member this.IsUnion (name:string) =
        match this.TryLookup name with
        | Some definition ->
            match definition.Element with
            | Element.Union(_) ->true
            | _ ->false
        | None ->
            false
                    
    member this.Parse (text:string) =
        
        let source =
            text.Split( '\n' )
            |> Seq.map ( fun line -> 
                let idx = line.IndexOf( "//")
                if idx = -1 then 
                    line
                else 
                    line.Substring(0,idx) )
            |> String.concat "\n"
            
        let trackLocation (parser: Parser<Element, unit>): Parser<Definition, unit> =
            
            fun (stream : CharStream<unit>) ->
                
                let oldState =
                    stream.State
                    
                let parseResult =
                    parser stream
                    
                if parseResult.Status = Ok then
                    let newState = stream.State
                    let matchedText = stream.ReadFrom (oldState, true)
                    let result = Definition.Make( matchedText.Trim(), parseResult.Result)
                    Reply( result )
                else
                    Reply( parseResult.Status, parseResult.Error )

        let pMany = 
            many (trackLocation (Element.Parser (this :> ISchema)) ) 
                        
        match source |> run pMany with
        | ParserResult.Success(definitions,_,_) ->
            definitions |> Seq.iter ( fun definition -> this.AddOrUpdate definition )
            ParseResult.Success( definitions |> Array.ofList )
            
        | ParserResult.Failure(error,_,_) -> 
            ParseResult.Failure( ParseFailure.Make( source, error ) )
        
    member this.Order () =

        lock this <| fun _ ->
            
            let graph =
                Graph.Simple.Make()
                
            items.Values
            |> Seq.cast
            |> Seq.iter ( fun definition ->
                
                match definition.Element with
                | Element.Scalar(s) ->
                    if not <| s.IsBuiltIn then  
                        graph.Add( s.Name )
                    
                | Element.Enum(e) ->
                    graph.Add( e.Name )
                    
                | Element.Interface(i) ->
                    graph.Add(i.Name)

                    i.Fields
                    |> Seq.map ( fun field -> field.FieldType.Underlying )
                    |> Seq.filter ( fun ut -> not <| this.IsBuiltIn ut )
                    |> Seq.iter ( fun ut ->
                        graph.Add( i.Name, ut ) )

                | Element.Record(r) ->
                    graph.Add( r.Name )
                    
                    r.Fields
                    |> Seq.map ( fun field -> field.FieldType.Underlying )
                    |> Seq.filter ( fun ut -> not <| this.IsBuiltIn ut )
                    |> Seq.iter ( fun ut ->
                        graph.Add( r.Name, ut ) ) 
                         
                    r.Implements
                    |> Seq.iter ( fun implements ->
                        graph.Add( r.Name, implements ) )
                         
                | Element.Union(u) ->
                    graph.Add( u.Name )
                    
                    u.Cases 
                    |> Seq.iter ( fun case -> 
                        graph.Add( u.Name, case.FieldType.Underlying ) ) )
                
            graph.Sort()            
        
    interface System.IDisposable
        with
            member this.Dispose () =
                this.Dispose()
                
    interface ISchema
        with
            member this.Count =
                this.Count
                
            member this.Clear () =
                this.Clear()
                
            member this.Items =
                this.Items
                
            member this.IsBuiltIn name =
                this.IsBuiltIn name

            member this.IsRecord name =
                this.IsRecord name

            member this.IsScalar name =
                this.IsScalar name
            
            member this.IsEnum name =
                this.IsEnum name

            member this.IsUnion name =
                this.IsUnion name

            member this.IsInterface name =
                this.IsInterface name
                                                                
            member this.TryLookup name =
                this.TryLookup name
                
            member this.Parse text =
                this.Parse text
                
            member this.AddOrUpdate definition =
                this.AddOrUpdate definition
                
            member this.Remove name =
                this.Remove name
                
            member this.Order () =
                this.Order()
                
            [<CLIEvent>]
            member this.OnAddedOrUpdated =
                onAddedOrUpdated.Publish
            
            [<CLIEvent>]
            member this.OnRemoved =
                onRemoved.Publish 
                
                