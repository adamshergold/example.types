namespace Example.Types.Schema

type Scalar = {
    Name: string 
}
with
    static member Make( name ) =
        { Name = name }

    static member Parser
        with get () = 
    