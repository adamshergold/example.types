namespace Example.Types.Schema

open FParsec

[<AutoOpen>]
module private ExtentionsImpl =
    
    let str_ws s = pstring s .>> spaces

    let pipe6 p1 p2 p3 p4 p5 p6 f = 
        pipe5 p1 p2 p3 p4 (tuple2 p5 p6)
              (fun x1 x2 x3 x4 (x5, x6) -> f x1 x2 x3 x4 x5 x6)

    let pName : Parser<string,unit> =
        
        let isIdentifierFirstChar c =
                isLetter c || c = '_'
                
        let isIdentifierChar c =
            isLetter c || isDigit c || c = '_' || c = '.'
    
        many1Satisfy2L isIdentifierFirstChar isIdentifierChar "name" .>> spaces 

    let pFieldType : Parser<FieldType,unit> =
        
        let pFieldType =
            pipe2
                pName
                (opt (str_ws "!"))
                ( fun name isOptional ->
                    FieldType.Simple( name, isOptional.IsNone ) )
                
        let pArray =
            pipe2
                (between (pstring "[") (pstring "]") pFieldType)
                (opt (str_ws "!"))
                ( fun fieldType isOptional ->
                    FieldType.Array( fieldType, isOptional.IsNone ) )

        let pMap =
            pipe2
                (between (pstring "<") (pstring ">") pFieldType)
                (opt (str_ws "!"))
                ( fun fieldType isOptional ->
                    FieldType.Map( fieldType, isOptional.IsNone ) )

        let pSet =
            pipe2
                (between (pstring "{") (pstring "}") pFieldType)
                (opt (str_ws "!"))
                ( fun fieldType isOptional ->
                    FieldType.Set( fieldType, isOptional.IsNone ) )
                                    
        choice [
            pArray
            pMap
            pSet
            pFieldType
        ]
            
module Extensions =
    
    type Scalar
        with
            static member Make( name, isBuiltIn ) =
                { Name = name; IsBuiltIn = isBuiltIn}
        
            static member Make( name ) =
                { Name = name; IsBuiltIn = false }
    
            static member Parser 
                with get () =
                    
                    fun (schema:ISchema) ->
                        let pScalar : Parser<string,unit> = 
                            
                            let isIdentifierFirstChar c =
                                isLetter c || c = '_'
                                
                            let isIdentifierChar c =
                                isLetter c || isDigit c || c = '_' 
                        
                            many1Satisfy2L isIdentifierFirstChar isIdentifierChar "scalar" .>> spaces
                        
                        str_ws "scalar" >>. pScalar |>> (fun v -> Scalar.Make( v ) ) 

    type Enum
        with
            static member Make( name, cases ) =
                { Name = name; Cases = cases }
    
            static member Parser 
                with get () =
                    
                    fun (schema:ISchema) ->
                        let pCases = 
                            sepEndBy pName spaces
                            
                        let pEnum = 
                            pipe4
                                pName
                                (str_ws "{")
                                pCases
                                (str_ws "}")
                                (fun name _ cases _ ->
                                    Enum.Make( name, cases |> Array.ofSeq ) ) 
                                    
                        str_ws "enum" >>. pEnum 
                
    type Field
        with
            static member Make( name, fieldType ) =
                { Name = name; FieldType = fieldType }

            static member Parser
                with get () =
            
                    fun (schema:ISchema) ->
                        pipe3
                            pName
                            (str_ws ":")
                            pFieldType
                            ( fun name _ fieldType ->
                                Field.Make( name, fieldType ) )                         
        
    type Union
        with
            static member Make( name, cases ) : Union =
                { Name = name; Cases = cases }
                
            static member Parser
            
                with get () =
                
                    fun (schema:ISchema) ->
                    
                        let pCases = 
                            sepEndBy (Field.Parser schema)  (str_ws "|")
                        
                        let pUnionDef = 
                            pipe3
                                pName
                                (str_ws "=")
                                pCases
                                (fun name _ cases ->
                                    Union.Make( name, cases |> Array.ofSeq ) )     
                     
                        str_ws "union" >>. pUnionDef
                    
    type Record
        with
            static member Make( name, fields, implements ) =
                { Name = name; Fields = fields; Implements = implements }
                
            static member Parser
            
                with get () =
                
                    fun (schema:ISchema) ->
                    
                        let pFields = 
                            sepEndBy (Field.Parser schema) spaces
                        
                        let pImplements =  
                            str_ws "implements" >>. pName
                            
                        let pTypeDef = 
                            pipe5
                                pName
                                (opt pImplements)
                                (str_ws "{")
                                pFields
                                (str_ws "}")
                                (fun name impl _ fields _ ->
                                    let implements = 
                                        match impl with | Some v -> [| v |] | None -> Array.empty
                                    Record.Make( name, fields |> Array.ofSeq, implements ) )     
                     
                        str_ws "type" >>. pTypeDef 
            
    type Interface
        with
            static member Make( name, fields ) =
                { Name = name; Fields = fields }
                
            static member Parser
            
                with get () =
                
                    fun (schema:ISchema) ->
                        let pFields = 
                            sepEndBy (Field.Parser schema) spaces
                        
                        let pInterfaceDef = 
                            pipe4
                                pName
                                (str_ws "{")
                                pFields
                                (str_ws "}")
                                (fun name _ fields _ ->
                                    Interface.Make( name, fields |> Array.ofSeq ) )     
                     
                        str_ws "interface" >>. pInterfaceDef
                    
    type FieldType
        with
            member this.Immediate
                with get () =
                    match this with
                    | FieldType.Simple(name,_) -> None
                    | FieldType.Array(ft,_) -> Some ft
                    | FieldType.Set(ft,_) -> Some ft
                    | FieldType.Map(ft,_) -> Some ft
                                            
            member this.Underlying
                with get () =
                    let rec impl (ft:FieldType) =
                        match ft with
                        | FieldType.Simple(name,_) -> name
                        | FieldType.Array(ft,_) -> impl ft
                        | FieldType.Set(ft,_) -> impl ft
                        | FieldType.Map(ft,_) -> impl ft
                        
                    impl this
                                
            member this.IsOptional
                with get () =
                    match this with
                    | FieldType.Simple(_,isOptional) -> isOptional
                    | FieldType.Array(_,isOptional) -> isOptional
                    | FieldType.Set(_,isOptional) -> isOptional
                    | FieldType.Map(_,isOptional) -> isOptional
                    
    type Element
        with
            static member Parser
                with get () =
                    fun (schema:ISchema) ->
                        choice [
                            (Scalar.Parser schema)    |>> ( fun v -> Element.Scalar( v ) )
                            (Enum.Parser schema)      |>> ( fun v -> Element.Enum( v ) )
                            (Record.Parser schema)    |>> ( fun v -> Element.Record( v ) )
                            (Interface.Parser schema) |>> ( fun v -> Element.Interface( v ) )
                            (Union.Parser schema)     |>> ( fun v -> Element.Union( v ) )
                        ]
                        
            member this.Name
                with get () =
                    match this with
                    | Element.Scalar(v) -> v.Name
                    | Element.Enum(v) -> v.Name
                    | Element.Record(v) -> v.Name
                    | Element.Union(v) -> v.Name
                    | Element.Interface(v) -> v.Name
                                    