namespace Example.Types.Schema

[<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]
type Scalar = {
    Name : string
    IsBuiltIn : bool
}

and
    [<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]
    
    Record = {
        Name : string
        Fields : Field[]
        Implements : string[]
}

and
    [<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]
    
    Union = {
        Name : string
        Cases : Field[]
}

and
    [<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]
    
    Interface = {
        Name : string
        Fields : Field[] 
}

and [<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]

    Enum = {
        Name : string
        Cases : string[]
}

and
    [<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]
    
    Element =
    | Scalar of Scalar
    | Enum of Enum
    | Record of Record
    | Union of Union
    | Interface of Interface
    
and
    [<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]
    
    FieldType =
    | Simple of string * bool
    | Array of FieldType * bool
    | Map of FieldType * bool
    | Set of FieldType * bool
    
and
    [<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]
    
    Field = {
    Name: string
    FieldType : FieldType
}

[<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]
type Definition = {
    Source : string
    Element : Element
}    
with
    static member Make( source, element ) =
        { Source = source; Element = element }
    