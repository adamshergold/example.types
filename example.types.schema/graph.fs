namespace Example.Types.Schema

module Graph = 

    type Simple() =
    
        let nodes = 
            new System.Collections.Generic.Dictionary<string,System.Collections.Generic.HashSet<string>>()
            
    with
        override this.ToString() = 
            let edges = this.Edges() |> Seq.map ( fun (p,c) -> sprintf "(%s,%s)" p c ) |> String.concat ";"
            sprintf "Simple(%s)" edges
            
        static member Make( ) = 
            new Simple()
            
        member this.Count () =
            nodes.Count 
                
        member this.Add (node:string) = 
            lock this ( fun () ->
                match nodes.TryGetValue node with
                | true, _ -> ()
                | false, _ -> 
                    let hs = new System.Collections.Generic.HashSet<string>()
                    nodes.Add(node,hs) )
            
        member this.Add (parent,child) =
            this.Add(parent)  
            this.Add(child)
            if parent <> child then
                lock this ( fun () ->
                    match nodes.TryGetValue parent with
                    | true, hs ->
                        hs.Add( child ) |> ignore
                    | false, _ ->
                        let hs = new System.Collections.Generic.HashSet<string>()
                        hs.Add(child) |> ignore
                        nodes.Add(parent,hs) )
        
        member this.TryFindNoChildren () =
            
            lock this ( fun () ->
                nodes.Keys 
                |> Seq.tryPick ( fun k -> 
                    if nodes.Item(k).Count = 0 then Some k else None ) )
                
        member this.Nodes () =
            lock this ( fun () ->
                nodes |> Seq.map ( fun kvp -> kvp.Key ) |> Array.ofSeq )
                                
        member this.Edges () = 
            lock this ( fun () ->
                nodes.Keys
                |> Seq.map ( fun parent ->
                    nodes.Item(parent) |> Seq.map ( fun child -> parent, child ) )
                |> Seq.concat )
                
        member this.Remove (parent:string,child:string) = 
            lock this ( fun () ->
                nodes.Item(parent).Remove(child) |> ignore )

        member this.Remove (parent:string) = 
            lock this ( fun () ->
                nodes.Remove(parent) |> ignore )
                        
        member this.RemoveChild (child:string) = 
            lock this ( fun () ->
                nodes.Keys
                |> Seq.iter ( fun k -> 
                    let hs = nodes.Item(k)
                    if hs.Contains(child) then hs.Remove(child) |> ignore else () )
                    
                if nodes.Item(child).Count = 0 then
                    nodes.Remove(child) |> ignore
                else
                    () )
                    
        member this.Copy () = 
            let copy = Simple.Make()
            this.Nodes() |> Seq.iter ( fun node -> copy.Add( node ) )
            this.Edges() |> Seq.iter ( fun (parent,child) ->
                copy.Add (parent,child) )
            copy
            
        member this.Children (id:string) = 
            match nodes.TryGetValue id with
            | false, _ -> Array.empty 
            | true, hs -> hs |> Seq.cast |> Array.ofSeq
                         
        member this.Sort () =
        
            let result = new System.Collections.Generic.List<string>()
            
            let temp = this.Copy()
            
            while temp.Count() <> 0 do 
            
                match temp.TryFindNoChildren() with 
                | Some n ->
                    result.Add(n)
                    temp.RemoveChild n
                | None ->
                    temp.Nodes() 
                    |> Seq.iter ( fun node -> 
                        result.Add(node)
                        temp.Remove(node) )
            
            result |> Seq.toArray
                
            
        
                            