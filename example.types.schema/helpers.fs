namespace Example.Types.Schema

module Helpers =
    
    let IsParseSuccess (pr:ParseResult) =
        match pr with | ParseResult.Success(_) -> true | ParseResult.Failure(_) -> false
        
