namespace Example.Types.Schema

[<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]
type ParseFailure = {
    Source : string
    Error : string
}
with
    static member Make( source, error ) =
        { Source = source; Error = error }
        
        
[<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]        
type ParseResult =
    | Success of Definition[]
    | Failure of ParseFailure
    
[<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]    
type AddedOrUpdated = {
    Name : string
    Source : string
}
with
    static member Make( name, source ) =
        { Name = name; Source = source }

[<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]
type Removed = {
    Name : string
}
with
    static member Make( name ) =
        { Name = name }

[<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]    
type ISchema =
    inherit System.IDisposable
    
    abstract Count : int with get
    
    abstract Clear : unit -> unit
    
    abstract Items : seq<Definition> with get
    
    abstract TryLookup : string -> Definition option
    
    abstract IsBuiltIn : string -> bool
    
    abstract IsEnum : string -> bool
    
    abstract IsRecord : string -> bool
    
    abstract IsInterface : string -> bool
    
    abstract IsUnion : string -> bool
    
    abstract IsScalar : string -> bool
    
    abstract AddOrUpdate : Definition -> unit
    
    abstract Remove : string -> bool
    
    abstract Parse : string -> ParseResult
    
    abstract Order : unit -> string[]
    
    
    [<CLIEvent>]
    abstract OnAddedOrUpdated : IEvent<AddedOrUpdated>
    
    [<CLIEvent>]
    abstract OnRemoved : IEvent<Removed>

