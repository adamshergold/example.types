namespace Example.Types.Tools.Tests.Example

type IPet =
    inherit System.ICloneable
    abstract Name : string with get
    abstract NickName : string option with get