namespace Example.Types.Tools.Tests.Example

open Example.Serialisation
open Example.Serialisation.Binary
open Example.Serialisation.Json

type Marker = {
    Name : string
}
with
    static member Make( _Name ) =
        {
            Name = _Name
        }

    member this.Clone () = 
        let _result = {
            Name = this.Name 
        }
        _result 

    interface System.ICloneable 
        with 
            member this.Clone () = 
                box <| this.Clone()

    interface Example.Types.Tools.Tests.Example.IMarker
    
    interface ITypeSerialisable

module private Marker_Serdes = 

    let Binary_Serde = 
        { new ITypeSerde<Marker>
            with
                member this.TypeName =
                    "Example.Marker"

                member this.ContentType =
                    "binary"

                member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =
    
                    use bs = 
                        BinarySerialiser.Make( serde, stream, this.TypeName )
                    
                    bs.Write( v.Name )
                        
                member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =
                                        
                    use bds = 
                        BinaryDeserialiser.Make( serde, stream, this.TypeName )

                    let _Name = 
                        bds.ReadString()
                        
                    let result = 
                        {
                            Name = _Name
                        }
                        
                    result }

    let JSON_Serde = 
        { new ITypeSerde<Marker>
            with
                member this.TypeName =
                    "Example.Marker"

                member this.ContentType =
                    "json"

                member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =

                    use js =
                        JsonSerialiser.Make( serde, stream, this.ContentType )

                    js.WriteStartObject()
                    js.WriteProperty serde.Options.TypeProperty
                    js.WriteValue this.TypeName

                    js.WriteProperty "Name"
                    js.Serialise v.Name

                    js.WriteEndObject()

                member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =

                    use jds =
                        JsonDeserialiser.Make( serde, stream, this.ContentType, this.TypeName )

                    jds.Handlers.On "Name" ( jds.ReadString )

                    jds.Deserialise()

                    let result =
                        {
                            Name = jds.Handlers.TryItem<_>( "Name" ).Value
                        }

                    result }

type Marker with

    static member Binary_Serde = Marker_Serdes.Binary_Serde
    
    static member JSON_Serde = Marker_Serdes.JSON_Serde
