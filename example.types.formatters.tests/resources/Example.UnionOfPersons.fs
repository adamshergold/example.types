namespace Example.Types.Tools.Tests.Example

open Example.Serialisation
open Example.Serialisation.Binary
open Example.Serialisation.Json

type UnionOfPersons = 
    | Persons of Person[] 
with
    member this.Clone () = 
        let _result =
            match this with 
            | Persons( v ) -> Persons( v |> Array.map ( fun v -> v.Clone() ) )
        _result 

    interface System.ICloneable 
        with 
            member this.Clone () = 
                box <| this.Clone()

    interface ITypeSerialisable

module private UnionOfPersons_Serdes = 

    let Binary_Serde = 
        { new ITypeSerde<UnionOfPersons>
            with
                member this.TypeName =
                    "Example.UnionOfPersons"

                member this.ContentType =
                    "binary"

                member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =
    
                    use bs = 
                        BinarySerialiser.Make( serde, stream, this.TypeName )
                    
                    match v with 
                    | Persons( v ) ->
                        bs.Write( "Persons" )
                        bs.Write( (int32) v.Length )
                        v |> Seq.iter bs.Write
                        
                member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =
                                        
                    use bds = 
                        BinaryDeserialiser.Make( serde, stream, this.TypeName )

                    let result =
                        match bds.ReadString() with
                        | _ as v when v.Equals( "Persons", System.StringComparison.OrdinalIgnoreCase ) ->

                            let _v = 
                                bds.ReadArray<Example.Types.Tools.Tests.Example.Person>( bds.ReadRecord<Example.Types.Tools.Tests.Example.Person> )

                            UnionOfPersons.Persons( _v )

                        | _ as v ->
                            failwithf "Unexpected union case seen when deserialising UnionOfPersons: '%s'" v 
                            
                    result }     

    let JSON_Serde =
        { new ITypeSerde<UnionOfPersons>
            with
                member this.TypeName =
                    "Example.UnionOfPersons"

                member this.ContentType =
                    "json"

                member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =

                    use js =
                        JsonSerialiser.Make( serde, stream, this.ContentType )

                    js.WriteStartObject()
                    js.WriteProperty serde.Options.TypeProperty
                    js.WriteValue this.TypeName

                    match v with
                    | Persons( v ) ->
                        js.WriteProperty "Persons"
                        js.WriteStartArray()
                        v |> Seq.iter js.Serialise
                        js.WriteEndArray()

                    js.WriteEndObject()

                member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =

                    use jds =
                        JsonDeserialiser.Make( serde, stream, this.ContentType, this.TypeName )

                    jds.Handlers.On "Persons" ( jds.ReadArray<Example.Types.Tools.Tests.Example.Person>( jds.ReadRecord "Example.Person" ) )

                    jds.Deserialise()

                    let result =
                        if jds.Handlers.Has "Persons" then
                            UnionOfPersons.Persons( jds.Handlers.TryItem<_>( "Persons" ).Value )
                        else
                            failwithf "Unable to determine union case when deserialising [%s]" this.TypeName

                    result }

type UnionOfPersons with

    static member Binary_Serde = UnionOfPersons_Serdes.Binary_Serde

    static member JSON_Serde = UnionOfPersons_Serdes.JSON_Serde
