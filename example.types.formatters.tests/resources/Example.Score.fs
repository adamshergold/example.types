namespace Example.Types.Tools.Tests.Example

open Example.Serialisation
open Example.Serialisation.Binary
open Example.Serialisation.Json

type Score = {
    Mark : double
    Pass : bool
}
with
    static member Make( _Mark, _Pass ) =
        {
            Mark = _Mark
            Pass = _Pass
        }

    member this.Clone () = 
        let _result = {
            Mark = this.Mark
            Pass = this.Pass
        }
        _result 

    interface System.ICloneable 
        with 
            member this.Clone () = 
                box <| this.Clone()

    interface ITypeSerialisable

module private Score_Serdes = 

    let Binary_Serde = 
        { new ITypeSerde<Score>
            with
                member this.TypeName =
                    "Example.Score"

                member this.ContentType =
                    "binary"

                member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =
    
                    use bs = 
                        BinarySerialiser.Make( serde, stream, this.TypeName )
                    
                    bs.Write( v.Mark )
                    
                    bs.Write( v.Pass ) 
                        
                member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =
                                        
                    use bds = 
                        BinaryDeserialiser.Make( serde, stream, this.TypeName )

                    let _Mark = 
                        bds.ReadDouble() 
                        
                    let _Pass = 
                        bds.ReadBool()
                     
                    let result = 
                        {
                            Mark = _Mark
                            Pass = _Pass 
                        }
                    
                    result }
                    
    let JSON_Serde = 
        { new ITypeSerde<Score>
            with
                member this.TypeName =
                    "Example.Score"

                member this.ContentType =
                    "json"

                member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =

                    use js =
                        JsonSerialiser.Make( serde, stream, this.ContentType )

                    js.WriteStartObject()
                    js.WriteProperty serde.Options.TypeProperty
                    js.WriteValue this.TypeName

                    js.WriteProperty "Mark"
                    js.Serialise v.Mark
                    
                    js.WriteProperty "Pass"
                    js.Serialise v.Pass
                    
                    js.WriteEndObject()

                member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =

                    use jds =
                        JsonDeserialiser.Make( serde, stream, this.ContentType, this.TypeName )

                    jds.Handlers.On "Mark" ( jds.ReadDouble )
                    jds.Handlers.On "Pass" ( jds.ReadBool )

                    jds.Deserialise()

                    let result =
                        {
                            Mark = jds.Handlers.TryItem<_>( "Mark" ).Value
                            Pass = jds.Handlers.TryItem<_>( "Pass" ).Value
                        }

                    result }

type Score with

    static member Binary_Serde = Score_Serdes.Binary_Serde
    
    static member JSON_Serde = Score_Serdes.JSON_Serde
