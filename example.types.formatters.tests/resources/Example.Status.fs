namespace Example.Types.Tools.Tests.Example

open Example.Serialisation
open Example.Serialisation.Binary
open Example.Serialisation.Json

type Status = 
    | Single of Single
    | Married of Married
with
    member this.Clone () = 
        let _result =
            match this with 
            | Single( v ) -> Single( v.Clone() )
            | Married( v ) -> Married( v.Clone() ) 
        _result 

    interface System.ICloneable 
        with 
            member this.Clone () = 
                box <| this.Clone()

    interface ITypeSerialisable

module private Status_Serdes = 

    let Binary_Serde = 
        { new ITypeSerde<Status>
            with
                member this.TypeName =
                    "Example.Status"

                member this.ContentType =
                    "binary"

                member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =
    
                    use bs = 
                        BinarySerialiser.Make( serde, stream, this.TypeName )
                    
                    match v with 
                    | Single( v ) -> 
                        bs.Write( "Single" )
                        bs.Write( v )
                        
                    | Married( v ) ->
                        bs.Write( "Married" )
                        bs.Write( v )
                        
                member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =
                                        
                    use bds = 
                        BinaryDeserialiser.Make( serde, stream, this.TypeName )

                    let result =
                        match bds.ReadString() with
                        | _ as v when v.Equals( "Single", System.StringComparison.OrdinalIgnoreCase ) ->
                            
                            let _v =
                                bds.ReadRecord<Example.Types.Tools.Tests.Example.Single>()
                                
                            Status.Single( _v )
                            
                        | _ as v when v.Equals( "Married", System.StringComparison.OrdinalIgnoreCase ) ->
                            
                            let _v =
                                bds.ReadRecord<Example.Types.Tools.Tests.Example.Married>()
                                
                            Status.Married( _v )
                            
                        | _ as v ->
                            failwithf "Unexpected union case seen when deserialising Status: '%s'" v

                    result } 
                        
    let JSON_Serde =
        { new ITypeSerde<Status>
            with
                member this.TypeName =
                    "Example.Status"

                member this.ContentType =
                    "json"

                member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =

                    use js =
                        JsonSerialiser.Make( serde, stream, this.ContentType )

                    js.WriteStartObject()
                    js.WriteProperty serde.Options.TypeProperty
                    js.WriteValue this.TypeName

                    match v with
                    | Single( v ) ->
                        js.WriteProperty "Single"
                        js.Serialise v

                    | Married( v ) ->
                        js.WriteProperty "Married"
                        js.Serialise v

                    js.WriteEndObject()

                member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =

                    use jds =
                        JsonDeserialiser.Make( serde, stream, this.ContentType, this.TypeName )

                    jds.Handlers.On "Single" ( jds.ReadRecord "Example.Single" )
                    jds.Handlers.On "Married" ( jds.ReadRecord "Example.Married" )

                    jds.Deserialise()

                    let result =
                        if jds.Handlers.Has "Single" then
                            Status.Single( jds.Handlers.TryItem<_>( "Single" ).Value )
                        else if jds.Handlers.Has "Married" then
                            Status.Married( jds.Handlers.TryItem<_>( "Married" ).Value )
                        else
                            failwithf "Unable to determine union case when deserialising [%s]" this.TypeName

                    result }

type Status with

    static member Binary_Serde = Status_Serdes.Binary_Serde
    
    static member JSON_Serde = Status_Serdes.JSON_Serde
