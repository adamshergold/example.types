namespace Example.Types.Tools.Tests.Example

open Example.Serialisation
open Example.Serialisation.Binary
open Example.Serialisation.Json

type Person = {
    Name : string
    Address : Address
    Phone : Phone option
    Scores : Map<string,Score>
    Pets : IPet[] option
    Status : Status
    Hobbies : Set<Hobby>
}
with
    static member Make( _Name, _Address, _Phone, _Scores, _Pets, _Status, _Hobbies ) =
        {
            Name = _Name
            Address = _Address
            Phone = _Phone
            Scores = _Scores
            Pets = _Pets
            Status = _Status
            Hobbies = _Hobbies
        }

    member this.Clone () = 
        let _result = {
            Name = this.Name 
            Address = this.Address.Clone() 
            Phone = this.Phone |> Option.map ( fun v -> v.Clone() )
            Scores = this.Scores |> Map.map ( fun k v -> v.Clone() )
            Pets = this.Pets |> Option.map ( fun v -> v |> Array.map ( fun v -> v.Clone() :?> IPet ) )
            Status = this.Status.Clone()
            Hobbies = this.Hobbies |> Set.map ( fun v -> v )
        }
        _result 

    interface System.ICloneable 
        with 
            member this.Clone () = 
                box <| this.Clone()

    interface ITypeSerialisable

module private Person_Serdes = 

    let Binary_Serde = 
        { new ITypeSerde<Person>
            with
                member this.TypeName =
                    "Example.Person"

                member this.ContentType =
                    "binary"

                member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =
    
                    use bs = 
                        BinarySerialiser.Make( serde, stream, this.TypeName )
                    
                    bs.Write( v.Name )
                    
                    bs.Write( v.Address ) 
                        
                    bs.Write( v.Phone.IsSome )
                    if v.Phone.IsSome then 
                        bs.Write( v.Phone.Value )
                        
                    bs.Write( (int32) v.Scores.Count )                        
                    v.Scores
                    |> Map.iter ( fun k v ->
                        bs.Write( k )
                        bs.Write( v )
                    )
                
                    bs.Write( v.Pets.IsSome )
                    if v.Pets.IsSome then 
                        bs.Write( (int32) v.Pets.Value.Length )
                        v.Pets.Value |> Seq.iter bs.Write

                    bs.Write( v.Status )
                    
                    bs.Write( (int32) v.Hobbies.Count )
                    v.Hobbies |> Seq.iter bs.Write            
                                                                                            
                member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =
                                        
                    use bds = 
                        BinaryDeserialiser.Make( serde, stream, this.TypeName )

                    let _Name = 
                        bds.ReadString() 
                        
                    let _Address = 
                        bds.ReadRecord<Example.Types.Tools.Tests.Example.Address>()
                     
                    let _Phone =
                        if bds.ReadBoolean() then 
                            Some <| bds.ReadRecord<Example.Types.Tools.Tests.Example.Phone>()
                        else 
                            None 
                            
                    let _Scores = 
                        bds.ReadMap<Example.Types.Tools.Tests.Example.Score>( bds.ReadRecord<Example.Types.Tools.Tests.Example.Score> )
                    
                    let _Pets = 
                        if bds.ReadBoolean() then 
                            Some <| bds.ReadArray<Example.Types.Tools.Tests.Example.IPet>( bds.ReadInterface<Example.Types.Tools.Tests.Example.IPet> ) 
                        else 
                            None 
                                    
                    let _Status = 
                        bds.ReadUnion<Example.Types.Tools.Tests.Example.Status>()
                        
                    let _Hobbies = 
                        bds.ReadSet<Example.Types.Tools.Tests.Example.Hobby>( bds.ReadEnum<Example.Types.Tools.Tests.Example.Hobby> )
                                            
                    let result = 
                        {
                            Name = _Name
                            Address = _Address
                            Phone = _Phone 
                            Scores = _Scores
                            Pets = _Pets
                            Status = _Status
                            Hobbies = _Hobbies
                        }
                    
                    result }
                    
    let JSON_Serde = 
        { new ITypeSerde<Person>
            with
                member this.TypeName =
                    "Example.Person"

                member this.ContentType =
                    "json"

                member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =

                    use js =
                        JsonSerialiser.Make( serde, stream, this.ContentType )

                    js.WriteStartObject()
                    js.WriteProperty serde.Options.TypeProperty
                    js.WriteValue this.TypeName

                    js.WriteProperty "Name"
                    js.Serialise v.Name
                    
                    js.WriteProperty "Address"
                    js.Serialise v.Address
                    
                    if v.Phone.IsSome then
                        js.WriteProperty "Phone"
                        js.Serialise v.Phone.Value
                    
                    js.WriteProperty "Scores"
                    js.WriteStartObject()
                    v.Scores
                    |> Map.iter ( fun k v ->
                        js.WriteProperty (k.ToString())
                        js.Serialise v
                    )
                    js.WriteEndObject()
                    
                    if v.Pets.IsSome then
                        js.WriteProperty "Pets"
                        js.WriteStartArray()
                        v.Pets.Value |> Seq.iter js.Serialise
                        js.WriteEndArray()
                    
                    js.WriteProperty "Status"
                    js.Serialise v.Status
                    
                    js.WriteProperty "Hobbies"
                    js.WriteStartArray()
                    v.Hobbies |> Seq.iter js.Serialise
                    js.WriteEndArray()
                    
                    js.WriteEndObject()

                member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =

                    use jds =
                        JsonDeserialiser.Make( serde, stream, this.ContentType, this.TypeName )

                    jds.Handlers.On "Name" ( jds.ReadString )
                    jds.Handlers.On "Address" ( jds.ReadRecord "Example.Address" )
                    jds.Handlers.On "Phone" ( jds.ReadRecord "Example.Phone" )
                    jds.Handlers.On "Scores" ( jds.ReadMap<Example.Types.Tools.Tests.Example.Score>( jds.ReadRecord "Example.Score" ) )
                    jds.Handlers.On "Pets" ( jds.ReadArray<Example.Types.Tools.Tests.Example.IPet>( jds.ReadInterface "Example.IPet" ) )
                    jds.Handlers.On "Status" ( jds.ReadUnion "Example.Status" )
                    jds.Handlers.On "Hobbies" ( jds.ReadSet<Example.Types.Tools.Tests.Example.Hobby>( jds.ReadEnum<Example.Types.Tools.Tests.Example.Hobby> ) )

                    jds.Deserialise()

                    let result =
                        {
                            Name = jds.Handlers.TryItem<_>( "Name" ).Value
                            Address = jds.Handlers.TryItem<_>( "Address" ).Value
                            Phone = jds.Handlers.TryItem<_>( "Phone" )
                            Scores = jds.Handlers.TryItem<_>( "Scores" ).Value
                            Pets = jds.Handlers.TryItem<_>( "Pets" )
                            Status = jds.Handlers.TryItem<_>( "Status" ).Value
                            Hobbies = jds.Handlers.TryItem<_>( "Hobbies" ).Value
                        }

                    result }

type Person with

    static member Binary_Serde = Person_Serdes.Binary_Serde

    static member JSON_Serde = Person_Serdes.JSON_Serde
