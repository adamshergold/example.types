namespace Example.Types.Tools.Tests.Example

open Example.Serialisation
open Example.Serialisation.Binary
open Example.Serialisation.Json
open NodaTime

type All = {
    TheSerialisable : ITypeSerialisable
    TheLocalDateTime : LocalDateTime
}
with
    static member Make( _TheSerialisable, _TheLocalDateTime ) =
        {
            TheSerialisable = _TheSerialisable
            TheLocalDateTime = _TheLocalDateTime
        }

    member this.Clone () = 
        let _result = {
            TheSerialisable = match this.TheSerialisable with | :? System.ICloneable as ic -> ic.Clone() :?> ITypeSerialisable | _ -> failwithf "Not cloneable!"
            TheLocalDateTime = this.TheLocalDateTime
        }
        _result 

    interface System.ICloneable 
        with 
            member this.Clone () = 
                box <| this.Clone()

    interface ITypeSerialisable

module private All_Serdes = 

    let Binary_Serde = 
        { new ITypeSerde<All>
            with
                member this.TypeName =
                    "Example.All"

                member this.ContentType =
                    "binary"

                member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =
    
                    use bs = 
                        BinarySerialiser.Make( serde, stream, this.TypeName )
                    
                    bs.Write( v.TheSerialisable )

                    bs.Write( v.TheLocalDateTime )
                        
                member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =
                                        
                    use bds = 
                        BinaryDeserialiser.Make( serde, stream, this.TypeName )

                    let _TheSerialisable = 
                        bds.ReadITypeSerialisable()

                    let _TheLocalDateTime = 
                        bds.ReadLocalDateTime()
                    
                    let result = 
                        {
                            TheSerialisable = _TheSerialisable
                            TheLocalDateTime = _TheLocalDateTime 
                        }
                    
                    result }

    let JSON_Serde = 
        { new ITypeSerde<All>
            with
                member this.TypeName =
                    "Example.All"

                member this.ContentType =
                    "json"

                member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =

                    use js =
                        JsonSerialiser.Make( serde, stream, this.ContentType )

                    js.WriteStartObject()
                    js.WriteProperty serde.Options.TypeProperty
                    js.WriteValue this.TypeName

                    js.WriteProperty "TheSerialisable"
                    js.Serialise v.TheSerialisable

                    js.WriteProperty "TheLocalDateTime"
                    js.Serialise v.TheLocalDateTime
                    
                    js.WriteEndObject()

                member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =

                    use jds =
                        JsonDeserialiser.Make( serde, stream, this.ContentType, this.TypeName )

                    jds.Handlers.On "TheSerialisable" ( jds.ReadITypeSerialisable )
                    jds.Handlers.On "TheLocalDateTime" ( jds.ReadLocalDateTime )

                    jds.Deserialise()

                    let result =
                        {
                            TheSerialisable = jds.Handlers.TryItem<_>( "TheSerialisable" ).Value
                            TheLocalDateTime = jds.Handlers.TryItem<_>( "TheLocalDateTime" ).Value
                        }

                    result }

type All with

    static member Binary_Serde = All_Serdes.Binary_Serde

    static member JSON_Serde = All_Serdes.JSON_Serde
