namespace Example.Types.Tools.Tests.Example

type Hobby =
    | Fishing = 0
    | Knitting = 1
    | Golf = 2