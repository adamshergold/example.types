namespace Example.Types.Formatters.Tests

open Xunit
open Xunit.Abstractions 
 
open Example.Types.Schema

open Example.Types.Formatters.FSharp

type FSharpHelpersShould( testOutputHelper:ITestOutputHelper ) =
                
    let logger = 
        Logging.CreateLogger testOutputHelper 
                            
    member val TestOutputHelper = testOutputHelper
    
    [<Theory>]
    [<InlineData("A.B.C","C")>]
    [<InlineData("A","A")>]
    member this.``GiveGoodShortNameAnswer`` (name:string,expected:string) =
        Assert.Equal( expected, Helpers.ShortName name )

    [<Theory>]
    [<InlineData("A.B.C","A.B")>]
    [<InlineData("A","")>]
    member this.``GiveGoodNamePrefixAnswer`` (name:string,expected:string) =
        Assert.Equal( expected, Helpers.NamePrefix name )
            