namespace Example.Types.Formatters.Tests

open Microsoft.Extensions.Logging

open Example.Types.Formatters

module Helpers =
    
    let LogFormat (logger:ILogger) (format:Format) =
        
        match format.ContentType with
        | "text" ->
            let text =
                format.Body |> System.Text.Encoding.UTF8.GetString
                
            let lines =
                text.Split( '\n' )
                
            lines
            |> Seq.iteri ( fun idx line ->
                logger.LogInformation( sprintf "[%3d] [%s]" idx line ) )
            
        | _ ->
            failwithf "Do not know how to format '%s'" format.ContentType
            
    let CompareFormat (logger:ILogger) (expected:Format) (actual:Format) =
        
        if expected.ContentType <> actual.ContentType then
            logger.LogError( "Content Type mis-match: expected {Expected} vs actual {Actual}", expected.ContentType, actual.ContentType )
            false
        else
            match expected.ContentType with
            | "text" ->
                
                let expected =
                    (expected.Body |> System.Text.Encoding.UTF8.GetString).Split( '\n' )
        
                let actual =
                    (actual.Body |> System.Text.Encoding.UTF8.GetString).Split( '\n' )
                    
                let nLines =
                    min expected.Length actual.Length
                    
                let lineByLine =
                    seq { 0 ..nLines-1 }
                    |> Seq.fold ( fun acc idx ->
                        let et = expected.[idx].TrimEnd()
                        let at = actual.[idx].TrimEnd()
                        if acc && (not <| et.Equals( at, System.StringComparison.Ordinal )) then
                            logger.LogError( "Mismatch at line {Line}", idx )
                            logger.LogError( "Expected (from file) [{Expected}]", et )
                            logger.LogError( "Actual   (generated) [{Actual}]", at )
                            false
                        else
                            acc ) true
                
                (expected.Length = actual.Length) && lineByLine
            | _ ->
                failwithf "Don't know how to compare content type '%s'" expected.ContentType
        