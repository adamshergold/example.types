namespace Example.Types.Formatters.Tests

open Microsoft.Extensions.Logging

open Xunit
open Xunit.Abstractions 
 
open Example.Types.Schema
open Example.Types.Schema.Extensions

open Example.Types.Formatters
open Example.Types.Formatters.FSharp

type TestCase = {
    Schema : ISchema 
    Name : string 
    Resource : string -> Format 
}
with 
    override this.ToString() = 
        this.Name 
        
    static member Make( schema, name, resource ) = 
        { Schema = schema; Name = name; Resource = resource }
            
type FSharpFormatterTestData private () as this =

    let prefix = 
        "example.types.formatters.tests.resources." 

    let definitions =
        lazy 
            this.GetType().Assembly.GetManifestResourceNames()
            |> Seq.filter ( fun name -> 
                name.EndsWith(".def") )
            |> Seq.map ( fun name ->
                name.Replace(".def","").Replace( prefix, "" ) )

    let resource (name:string) =
            
        let names = 
            System.Reflection.Assembly.GetCallingAssembly().GetManifestResourceNames()
            
        try     
            let stream =    
                System.Reflection.Assembly.GetCallingAssembly().GetManifestResourceStream( prefix + name )
    
            use tr = 
                new System.IO.StreamReader(stream)
    
            Format.Make( "text", name, tr.ReadToEnd() |> System.Text.Encoding.UTF8.GetBytes )
        with 
        | _ as ex ->
            failwithf "Error loading resource '%s' : '%s" name ex.Message
            
    static let instance = new FSharpFormatterTestData()

    member val Definitions = definitions 
    
with
    member this.Resource (name:string) = 
        resource name 
                
    static member Instance
        with get () = instance 
 
    static member TestData 
        with get () = 
        
            let me = 
                FSharpFormatterTestData.Instance

            let schema =
                
                let options =
                    SchemaOptions.Default
                    
                Schema.Make( options )
                
            let nameAndExpecteds =                
                me.Definitions.Value
                |> Seq.choose ( fun name ->
                
                    let definitionString = 
                        let format = me.Resource (sprintf "%s.def" name)
                        format.Body |> System.Text.Encoding.UTF8.GetString
                        
                    let parseResult =  
                        schema.Parse definitionString 
                                            
                    let definition =
                        match parseResult with 
                        | ParseResult.Success(defs) ->
                            if defs.Length <> 1 then
                                failwithf "No parse result for '%s'" definitionString
                            else    
                                defs.[0]
                        | ParseResult.Failure(pf) ->
                            failwithf "Failed to parse [%s] : [%s]" definitionString pf.Error
                            
                    let item = 
                        definition.Element.Name
                                                  
                    Some item ) 

                |> Array.ofSeq                        
                        
            
            nameAndExpecteds                   
            |> Seq.map ( fun name -> 
                [| box(TestCase.Make( schema, name, me.Resource ) ) |] ) 
            |> Array.ofSeq                                    
                                                                                             
    
type FormatterFSharpShould( testOutputHelper:ITestOutputHelper ) =
                
    let logger = 
        Logging.CreateLogger testOutputHelper 
                            
    member val TestOutputHelper = testOutputHelper
                        
    [<Theory>]
    [<MemberData("TestData",MemberType=typeof<FSharpFormatterTestData>)>]
    member this.ProduceCodeThatMatches (test:TestCase) =

        let options =
            { Options.Default with
                  Namespace = "Example.Types.Tools.Tests"
                  Serdes = [| Serde.JSON; Serde.Binary |] }

        let sut = 
            Formatter.Make( options )     
        
        let results = 
            sut.Format test.Schema test.Name                                         
        
        results
        |> Seq.iter ( fun actualFormat ->
                         
            testOutputHelper.WriteLine <| sprintf "Name = %s" actualFormat.Name 
                                      
            let expectedFormat = 
                test.Resource actualFormat.Name 
                         
            let isSame =
                Helpers.CompareFormat logger expectedFormat actualFormat 
            
            logger.LogInformation( "Actual Output" )
            Helpers.LogFormat logger actualFormat

            Assert.True( isSame ) )

            