namespace Example.Types.Formatters.FSharp

open Example.Types.Schema

open Example.Types.Formatters
open Example.Types.Formatters.FSharp
open Example.Types.Formatters.FSharp.Helpers

module Enum =
    
    let Format (options:Options) (enum:Enum) =
        
        let nameSpace =
            Helpers.NamespaceFromName options.Namespace enum.Name
            
        let shortName =
            Helpers.ShortName enum.Name
            
        let lines =
            seq {
                yield Source.Make( 0, sprintf "namespace %s" nameSpace )
                
                yield Source.Empty
                
                yield Source.Make( 0, sprintf "type %s =" shortName )
                
                yield!
                    enum.Cases |> Seq.mapi ( fun idx case ->
                        Source.Make( 1, sprintf "| %s = %d" case idx ) )
            }
            
        let text =
            lines |> Seq.map Helpers.SourceToText |> String.concat "\n"
            
        let result =
            
            let name =
                sprintf "%s.fs" enum.Name
                
            Format.Make( "text", name, text |> System.Text.Encoding.UTF8.GetBytes )
        
        result |> Array.singleton
        
                                