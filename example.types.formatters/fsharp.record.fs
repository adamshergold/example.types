namespace Example.Types.Formatters.FSharp

open Example.Types.Schema
open Example.Types.Schema.Extensions

open Example.Types.Formatters
open Example.Types.Formatters.FSharp
open Example.Types.Formatters.FSharp.Helpers

module Record =
    
    let Format (schema:ISchema) (options:Options) (record:Record) =
        
        let nameSpace =
            Helpers.NamespaceFromName options.Namespace record.Name
            
        let shortName =
            Helpers.ShortName record.Name
        
        let hasSerialisation =
            options.Serdes.Length > 0
            
        let namespacesToOpen =
            Helpers.NamespacesToOpen options
            
        let additionalFields =
            record.Implements
            |> Seq.map ( fun ifaceName ->
                match schema.TryLookup ifaceName with
                | Some definition ->
                    match definition.Element with
                    | Element.Interface(iface) ->
                        iface.Fields
                    | _ ->
                        failwithf "Type implements something that is not an interface '%s' : '%O' " ifaceName definition.Element
                | None ->
                    failwithf "Type implements something that is not defined: '%O'" ifaceName )
            |> Seq.concat
            |> Seq.distinct
            |> Array.ofSeq
            
        let allFields =
            Array.append additionalFields record.Fields
            
        let typeSpecificAdditionalNamespaces =
            allFields
            |> Seq.choose ( fun field ->
                let ut = field.FieldType.Underlying
                match options.AdditionalScalarToFSharpTypes.TryFind ut with
                | Some (_,ns) -> ns
                | None -> None )
            |> Array.ofSeq
            
        let lines =
            seq {
                yield Source.Make( 0, sprintf "namespace %s" nameSpace )
                
                yield Source.Empty
                
                if namespacesToOpen.Length > 0 || typeSpecificAdditionalNamespaces.Length > 0 then 
                    yield!
                        namespacesToOpen |> Seq.map ( fun ns -> Source.Make( 0, sprintf "open %s" ns ) )
                    yield!
                        typeSpecificAdditionalNamespaces |> Seq.map ( fun ns -> Source.Make( 0, sprintf "open %s" ns ) )
                                    
                    yield Source.Empty
                    
                if allFields.Length > 0 then 
                    yield Source.Make( 0, sprintf "type %s = {" shortName )
                    
                    yield!
                        allFields
                        |> Seq.map ( fun field ->
                            Source.Make( 1, sprintf "%s : %s" field.Name (Helpers.FieldTypeToFSharpType schema options nameSpace field.FieldType) ) )
                        
                    yield Source.Make( 0, "}" )
                    
                else
                    yield Source.Make( 0, sprintf "type %s () = class end" shortName )
                    
                yield Source.Make( 0, sprintf "with" )
                
                if allFields.Length > 0 then 
                    let args =
                        allFields |> Seq.map ( fun field -> sprintf "_%s" field.Name ) |> String.concat ", "
                        
                    yield Source.Make( 1, sprintf "static member Make( %s ) =" args )
                    
                    yield Source.Make( 2, sprintf "{" )
                    
                    yield!
                        allFields
                        |> Seq.map ( fun field ->
                            Source.Make( 3, sprintf "%s = _%s" field.Name field.Name ) )

                    yield Source.Make( 2, sprintf "}" )                        
                    
                    yield Source.Empty
                    
                else
                    yield Source.Make( 1, sprintf "static member Make() = " )
                    yield Source.Make( 2, sprintf "new %s()" shortName )
                    
                    yield Source.Empty
                    
                    yield Source.Make( 1, "override this.GetHashCode () =" )
                    yield Source.Make( 2, "0" )
                    yield Source.Empty

                    yield Source.Make( 1, "override this.Equals (other:obj) =" )
                    yield Source.Make( 2, "( this :> System.IComparable ).CompareTo( other ).Equals( 0 )" )
                    yield Source.Empty

                    yield Source.Make( 1, "interface System.IComparable" )
                    yield Source.Make( 2, "with" )
                    yield Source.Make( 3, "member this.CompareTo (other:obj) =" )
                    yield Source.Make( 4, "match other with" )
                    yield Source.Make( 4, sprintf "| :? %s -> 0" shortName )
                    yield Source.Make( 4, sprintf "| _ -> failwithf \"Cannot compare %s to '%%O'\" other" shortName )
                    yield Source.Empty
                                                        
                if options.Cloneable then
                    yield Source.Make( 1, "member this.Clone () =" )
                    
                    if allFields.Length > 0 then 
                        yield Source.Make( 2, "let _result = {" )
                        yield!
                            allFields
                            |> Seq.map ( fun field ->
                                let clone = Helpers.Clone schema nameSpace (sprintf "this.%s" field.Name) field.FieldType options
                                Source.Make( 3, sprintf "%s = %s" field.Name clone ) )
                        yield Source.Make( 2, "}")
                        yield Source.Make( 2, "_result" )
                    else
                        yield Source.Make( 2, sprintf "new %s()" shortName )
                        
                    yield Source.Empty
                    
                    yield Source.Make( 1, "interface System.ICloneable")
                    yield Source.Make( 2, "with" )
                    yield Source.Make( 3, "member this.Clone () =" )
                    yield Source.Make( 4, "box <| this.Clone()" )
                    yield Source.Empty
                    
                    
                if record.Implements.Length > 0 then
                    yield!
                        record.Implements
                        |> Seq.map ( fun ifaceName ->
                            
                            let definition =
                                schema.TryLookup ifaceName
                            
                            if definition.IsNone then
                                failwithf "type '%s' claims to implement interface '%s' that is not defined!" record.Name ifaceName
                                
                            let properties =
                                match definition.Value.Element with
                                | Element.Interface(r) -> r.Fields
                                | _ -> failwithf "type '%s' implements an interface '%s' which is not defined as such!" record.Name ifaceName
                                
                            seq {
                                yield Source.Make( 1, sprintf "interface %s" (FullyQualifiedTypeName options ifaceName) )
                                
                                if properties.Length > 0 then 
                                    yield Source.Make( 2, sprintf "with" )
                                    yield!
                                        properties
                                        |> Seq.map ( fun field ->
                                            Source.Make( 3, sprintf "member this.%s = this.%s" field.Name field.Name) )
                                        
                                yield Source.Empty
                            } )
                        |> Seq.concat
                  
                if hasSerialisation then
                    
                    yield Source.Make( 1, sprintf "interface ITypeSerialisable" )
                    yield Source.Empty
                    
                    yield Source.Make( 0, sprintf "module private %s_Serdes =" shortName )
                    yield Source.Empty
                    
                    if HasSerde options Serde.Binary then
                        
                        yield Source.Make( 1, sprintf "let %s_Serde =" (Helpers.SerdeName Serde.Binary) )
                        
                        yield Source.Make( 2, sprintf "{ new ITypeSerde<%s>" shortName )
                        yield Source.Make( 3, sprintf "with" )
                        
                        yield Source.Make( 4, sprintf "member this.TypeName =" )
                        yield Source.Make( 5, sprintf "\"%s\"" record.Name )
                        yield Source.Empty

                        yield Source.Make( 4, sprintf "member this.ContentType =" )
                        yield Source.Make( 5, sprintf "\"binary\"" )
                        yield Source.Empty

                        yield Source.Make( 4, sprintf "member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =" )
                        yield Source.Empty
                        yield Source.Make( 5, "use bs =")
                        yield Source.Make( 6, "BinarySerialiser.Make( serde, stream, this.TypeName )")
                        yield Source.Empty
                        
                        if allFields.Length > 0 then
                            yield!
                                allFields
                                |> Seq.map ( fun field -> Helpers.BinaryFieldSerialiser schema 5 options field None )
                                |> Seq.concat
                        else
                            yield Source.Make( 5, "()")
                            yield Source.Empty
                            
                        yield Source.Make( 4, sprintf "member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =" )
                        yield Source.Empty
                        yield Source.Make( 5, "use bds =")
                        yield Source.Make( 6, "BinaryDeserialiser.Make( serde, stream, this.TypeName )")
                        yield Source.Empty

                        yield!
                            allFields
                            |> Seq.map ( fun field -> Helpers.BinaryFieldDeserialiser schema 5 options nameSpace field None)
                            |> Seq.concat
                            
                        yield Source.Make( 5, "let result =")
                        
                        if allFields.Length > 0 then
                            
                            yield Source.Make( 6, "{" )
                                               
                            yield!
                                allFields
                                |> Seq.map ( fun field ->
                                    Source.Make( 7, sprintf "%s = _%s" field.Name field.Name ) )
                                
                            yield Source.Make( 6, "}" )
                            
                        else
                            yield Source.Make( 6, sprintf "new %s()" shortName )
                                                            
                        yield Source.Empty
                        yield Source.Make( 5, "result }" )
                        
                        yield Source.Empty
                        
                    if HasSerde options Serde.JSON then
                        yield Source.Make( 1, sprintf "let %s_Serde =" (Helpers.SerdeName Serde.JSON) )
                        
                        yield Source.Make( 2, sprintf "{ new ITypeSerde<%s>" shortName )
                        yield Source.Make( 3, sprintf "with" )
                        
                        yield Source.Make( 4, sprintf "member this.TypeName =" )
                        yield Source.Make( 5, sprintf "\"%s\"" record.Name )
                        yield Source.Empty

                        yield Source.Make( 4, sprintf "member this.ContentType =" )
                        yield Source.Make( 5, sprintf "\"json\"" )
                        yield Source.Empty

                        yield Source.Make( 4, sprintf "member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =" )
                        yield Source.Empty
                        yield Source.Make( 5, "use js =")
                        yield Source.Make( 6, "JsonSerialiser.Make( serde, stream, this.ContentType )")
                        yield Source.Empty
                        
                        yield Source.Make( 5, sprintf "js.WriteStartObject()" )
                        yield Source.Make( 5, sprintf "js.WriteProperty serde.Options.TypeProperty" )
                        yield Source.Make( 5, sprintf "js.WriteValue this.TypeName" )
                        yield Source.Empty
                        
                        yield!
                            allFields
                            |> Seq.map ( fun field -> Helpers.JsonFieldSerialiser schema 5 options field None)
                            |> Seq.concat
                        
                        yield Source.Make( 5, sprintf "js.WriteEndObject()" )
                        yield Source.Empty
                        
                        yield Source.Make( 4, sprintf "member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =" )
                        yield Source.Empty
                        yield Source.Make( 5, "use jds =")
                        yield Source.Make( 6, "JsonDeserialiser.Make( serde, stream, this.ContentType, this.TypeName )")
                        yield Source.Empty

                        if allFields.Length > 0 then 
                            yield!
                                allFields
                                |> Seq.map ( fun field -> Helpers.JsonFieldOnHandler schema 5 options nameSpace field )
                                |> Seq.concat
                            yield Source.Empty
                            
                        yield Source.Make( 5, "jds.Deserialise()")
                        yield Source.Empty
                        
                        yield Source.Make( 5, "let result =")
                        
                        if allFields.Length > 0 then
                            yield Source.Make( 6, "{" )
                            
                            yield!
                                allFields
                                |> Seq.map ( fun field ->
                                    let opt = if field.FieldType.IsOptional then "" else ".Value"
                                    Source.Make( 7, sprintf "%s = jds.Handlers.TryItem<_>( \"%s\" )%s" field.Name field.Name opt ) )
                                
                            yield Source.Make( 6, "}" )
                            yield Source.Empty 
                        else
                            yield Source.Make( 6, sprintf "new %s()" shortName )
                            yield Source.Empty
                            
                        yield Source.Make( 5, "result }" )
                        
                        yield Source.Empty
                        
                    
                    if hasSerialisation then
                        yield Source.Make( 0, sprintf "type %s with" shortName )
                        yield Source.Empty
                    
                        yield! 
                            options.Serdes
                            |> Seq.sortBy ( fun s -> s.ToString() )
                            |> Seq.map ( fun serde ->
                                let serdeName = Helpers.SerdeName serde
                                seq {
                                    yield Source.Make( 1, sprintf "static member %s_Serde = %s_Serdes.%s_Serde" serdeName shortName serdeName )
                                    yield Source.Empty
                                } )
                            |> Seq.concat
                            
            }
            
        let text =
            lines |> Seq.map Helpers.SourceToText |> String.concat "\n"
            
        let result =
            
            let name =
                sprintf "%s.fs" record.Name
                
            Format.Make( "text", name, text |> System.Text.Encoding.UTF8.GetBytes )
        
        result |> Array.singleton

