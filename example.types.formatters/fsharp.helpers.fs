namespace Example.Types.Formatters.FSharp

open Example.Types.Schema
open Example.Types.Schema.Extensions

open Example.Types.Formatters

module Helpers =
    
    let UpperCaseFirst (s:string) =
        if System.String.IsNullOrEmpty s then
            s
        else
            let chars = s.ToCharArray()
            chars.[0] <- System.Char.ToUpper( chars.[0] )
            new System.String(chars)
            
    type Source = {
        Ident : int
        Text : string
    }
    with
        static member Make( indent, text ) =
            { Ident = indent; Text = text }

        static member Empty
            with get () = Source.Make( 0, "" )
            
    let SourceToText (source:Source) =
        let indent = String.replicate (source.Ident) "    "
        sprintf "%s%s" indent (source.Text.TrimEnd())
        
    let NamePrefix (name:string) =
        let lastDot = name.LastIndexOf '.'
        if lastDot = -1 then "" else name.Substring( 0, lastDot )
        
    let ShortName (name:string) =
        let lastDot = name.LastIndexOf '.'
        if lastDot = -1 then name else name.Substring( lastDot + 1 )
        
    let NamespaceFromName (ns:string) (name:string) =
        let prefix = NamePrefix name
        if prefix.Length = 0 then 
            ns
        else
            sprintf "%s.%s" ns prefix

    let HasSerde (options:Options) (serde:Serde) =
        options.Serdes |> Array.exists ( fun name -> name.Equals( serde ) )
        
    let SerdeName (serde:Serde) =
        serde.ToString()
            
    let NamespacesToOpen (options:Options) =
        
        let additionalOpens =
            Map.ofSeq [
                Serde.JSON,   Set.ofList [ "Example.Serialisation"; "Example.Serialisation.Json" ]
                Serde.Binary, Set.ofList [ "Example.Serialisation"; "Example.Serialisation.Binary" ]
            ]
            
        options.Serdes
        |> Seq.choose additionalOpens.TryFind
        |> Seq.concat
        |> Set.ofSeq
        |> Set.union options.OpenNamespaces
        |> Set.toSeq
        |> Seq.sort
        |> Array.ofSeq
        
    let FullyQualifiedTypeName (options:Options) (name:string) =
        sprintf "%s.%s" options.Namespace name
        
    let NameToFSharpType (schema:ISchema) (options:Options) (currentNamespace:string option) (name:string) =
        
        if schema.IsScalar name then
            match name with
            | "Int" -> "int32"
            | "Float" -> "float"
            | "String" -> "string"
            | "Boolean" -> "bool"
            | "Int32" -> "int"
            | "Int64" -> "int64"
            | "Double" -> "double"
            | _ ->
                match options.AdditionalScalarToFSharpTypes.TryFind name with
                | Some (fst,_) -> fst
                | None -> failwithf "Unable to map '%s' to FSharpType" name
            
        else if schema.IsEnum name || schema.IsRecord name || schema.IsInterface name || schema.IsUnion name then
            let fqtn = FullyQualifiedTypeName options name
            if currentNamespace.IsNone then
                fqtn
            else
                fqtn.Replace( currentNamespace.Value + ".", "" )
        else
            failwithf "Do not know how to map '%s' to FSharpType" name
            
    let FieldTypeToFSharpType (schema:ISchema) (options:Options) (currentNamespace:string) (ft:FieldType) =
        
        let rec impl (ft:FieldType) =
            match ft with
            | FieldType.Simple( name, isOptional ) ->
                sprintf "%s%s" (NameToFSharpType schema options (Some currentNamespace) name ) (if isOptional then " option" else "")
                
            | FieldType.Array( ft, isOptional ) ->
                sprintf "%s[]%s" (impl ft) (if isOptional then " option" else "")

            | FieldType.Set( ft, isOptional ) ->
                sprintf "Set<%s>%s" (impl ft) (if isOptional then " option" else "")
                            
            | FieldType.Map( ft, isOptional ) ->
                sprintf "Map<string,%s>%s" (impl ft) (if isOptional then " option" else "")
                
        impl ft
        
    let ReadForScalar (options:Options) (name:string) =
        match name with
        | "Int" -> "ReadInt32"
        | "String" -> "ReadString"
        | "Float" -> "ReadFloat"
        | "Double" -> "ReadDouble"
        | "Boolean" -> "ReadBool"
        | _ ->
            match options.AdditionalScalarToFSharpTypes.TryFind name with
            | Some (fst,_) -> sprintf "Read%s" fst
            | None -> sprintf "Read%s" name

            
    let JsonFieldOnHandler (schema:ISchema) (indent:int) (options:Options) (currentNamespace:string) (field:Field) =

        let readerForName (name:string) =
            if schema.IsEnum name then
                sprintf "jds.ReadEnum<%s>" (FullyQualifiedTypeName options name)
            else if schema.IsRecord name then
                sprintf "jds.ReadRecord \"%s\"" name
            else if schema.IsInterface name then
                sprintf "jds.ReadInterface \"%s\"" name
            else if schema.IsUnion name then
                sprintf "jds.ReadUnion \"%s\"" name
            else    
                sprintf "jds.Read%s" (UpperCaseFirst <| (NameToFSharpType schema options (Some currentNamespace) name))
        
        let rec read (fieldType:FieldType) =
            
            let impl (tn:string) (name:string) (ft:FieldType) =
                let gt = NameToFSharpType schema options None name
                sprintf "jds.Read%s<%s>( %s )" tn gt (read ft)
                
            match fieldType with
            | FieldType.Simple(name,isOptional) ->
                readerForName name
                
            | FieldType.Array(innerFieldType,isOptional) ->
                match innerFieldType with 
                | FieldType.Simple(name,innerOptional) ->
                    impl "Array" name innerFieldType
                | _ ->
                    failwithf "Not supported '%O'" innerFieldType
                    
            | FieldType.Set(innerFieldType,isOptional) ->
                match innerFieldType with 
                | FieldType.Simple(name,innerOptional) ->
                    impl "Set" name innerFieldType
                | _ ->
                    failwithf "Not supported '%O'" innerFieldType
                    
            | FieldType.Map(innerFieldType,isOptional) ->
                match innerFieldType with 
                | FieldType.Simple(name,innerOptional) ->
                    impl "Map" name innerFieldType                
                | _ ->
                    failwithf "Not supported '%O'" innerFieldType
    
        seq {
            yield Source.Make( indent, sprintf "jds.Handlers.On \"%s\" ( %s )" field.Name (read field.FieldType) )
        }
            
    let JsonFieldSerialiser (schema:ISchema) (indent:int) (options:Options) (field:Field) (variableOverride:string option) =

        let varName = 
            match variableOverride with 
            | Some v -> v
            | None -> sprintf "v.%s" field.Name
            
        let indent', optionalSuffix =
            if field.FieldType.IsOptional then indent+1, ".Value" else indent, ""

        seq {
            if field.FieldType.IsOptional then
                yield Source.Make( indent, sprintf "if %s.IsSome then" varName )
                
            yield Source.Make( indent', sprintf "js.WriteProperty \"%s\"" field.Name )
            
            match field.FieldType with
            | FieldType.Simple(name,_) ->
                if schema.IsEnum name then
                    yield Source.Make( indent', sprintf "js.Serialise (%s%s.ToString())" varName optionalSuffix )
                else
                    yield Source.Make( indent', sprintf "js.Serialise %s%s" varName optionalSuffix )
            | FieldType.Array(ft,_)
            | FieldType.Set(ft,_) ->
                yield Source.Make( indent', sprintf "js.WriteStartArray()" )
                yield Source.Make( indent', sprintf "%s%s |> Seq.iter js.Serialise" varName optionalSuffix )
                yield Source.Make( indent', sprintf "js.WriteEndArray()" )
            | FieldType.Map(ft,_) ->
                yield Source.Make( indent',   sprintf "js.WriteStartObject()" )
                yield Source.Make( indent',   sprintf "%s%s" varName optionalSuffix )
                yield Source.Make( indent',   sprintf "|> Map.iter ( fun k v ->" )
                yield Source.Make( indent'+1, sprintf "js.WriteProperty (k.ToString())" )
                yield Source.Make( indent'+1, sprintf "js.Serialise v" )
                yield Source.Make( indent',   sprintf ")" )
                
                yield Source.Make( indent', sprintf "js.WriteEndObject()" )
                
                
            yield Source.Empty                
        }
        
    let BinaryFieldSerialiser (schema:ISchema) (indent:int) (options:Options) (field:Field) (variableOverride:string option) =
        
        let varName = 
            match variableOverride with 
            | Some v -> v
            | None -> sprintf "v.%s" field.Name
            
        let indent', optionalSuffix =
            if field.FieldType.IsOptional then indent+1, ".Value" else indent, ""
            
        seq {
            if field.FieldType.IsOptional then
                yield Source.Make( indent,   sprintf "bs.Write( %s.IsSome )" varName )
                yield Source.Make( indent,   sprintf "if %s.IsSome then" varName )
                
            match field.FieldType with
            | FieldType.Simple( name, _) ->
                if schema.IsEnum name then
                    yield Source.Make( indent', sprintf "bs.Write( %s%s.ToString() )" varName optionalSuffix)
                else
                    yield Source.Make( indent', sprintf "bs.Write( %s%s )" varName optionalSuffix)
            | FieldType.Array( ft, _ ) ->
                yield Source.Make( indent', sprintf "bs.Write( (int32) %s%s.Length )" varName optionalSuffix)
                yield Source.Make( indent', sprintf "%s%s |> Seq.iter bs.Write" varName optionalSuffix)
            | FieldType.Set( ft, _ ) ->
                yield Source.Make( indent', sprintf "bs.Write( (int32) %s%s.Count )" varName optionalSuffix)
                yield Source.Make( indent', sprintf "%s%s |> Seq.iter bs.Write" varName optionalSuffix)
            | FieldType.Map( ft, _ ) ->
                yield Source.Make( indent', sprintf "bs.Write( (int32) %s%s.Count )" varName optionalSuffix)
                yield Source.Make( indent', sprintf "%s%s" varName optionalSuffix)
                yield Source.Make( indent', sprintf "|> Map.iter ( fun k v ->" )
                yield Source.Make( indent' + 1, sprintf "bs.Write( k )" )
                yield Source.Make( indent' + 1, sprintf "bs.Write( v )" )
                yield Source.Make( indent', sprintf ")" )
            
            yield Source.Empty
        }
        
    let BinaryFieldDeserialiser (schema:ISchema) (indent:int) (options:Options) (currentNamespace:string) (field:Field) (variableOverride:string option) =
        
        let varName = 
            match variableOverride with 
            | Some v -> v
            | None -> sprintf "_%s" field.Name
        
        let indent', optionalSuffix, optionalPrefix =
            if field.FieldType.IsOptional then indent + 2, ".Value", "Some <| " else indent + 1, "", ""
            
        let readerForName (name:string) =
            if schema.IsEnum name then
                sprintf "ReadEnum<%s>" (FullyQualifiedTypeName options name)
            else if schema.IsRecord name then
                sprintf "ReadRecord<%s>" (FullyQualifiedTypeName options name)
            else if schema.IsInterface name then
                sprintf "ReadInterface<%s>" (FullyQualifiedTypeName options name)
            else if schema.IsUnion name then
                sprintf "ReadUnion<%s>" (FullyQualifiedTypeName options name)
            else    
                ReadForScalar options name
                
        seq {
            yield Source.Make( indent, sprintf "let %s =" varName )
            
            if field.FieldType.IsOptional then
                yield Source.Make( indent + 1, sprintf "if bds.ReadBoolean() then " )
                
            let impl (tn:string) (name:string) =
                let gt = NameToFSharpType schema options None name
                Source.Make( indent', sprintf "%sbds.Read%s<%s>( bds.%s )" optionalPrefix tn gt (readerForName name))
 
            match field.FieldType with
            | FieldType.Simple( name, _ ) ->
                yield Source.Make( indent', sprintf "%sbds.%s()" optionalPrefix (readerForName name) )
                
            | FieldType.Array( ft, _ ) ->
                match ft with
                | FieldType.Simple( name, isOptional ) ->
                    yield impl "Array" name
                | _ ->
                    failwithf "Not supported '%O'" ft
                    
            | FieldType.Set( ft, _ ) ->
                match ft with
                | FieldType.Simple( name, isOptional ) ->
                    yield impl "Set" name
                | _ ->
                    failwithf "Not supported '%O'" ft
                    
            | FieldType.Map( ft, _ ) ->
                match ft with
                | FieldType.Simple( name, isOptional ) ->
                    yield impl "Map" name
                | _ ->
                    failwithf "Not supported '%O'" ft
                
            if field.FieldType.IsOptional then
                yield Source.Make( indent + 1, "else" )
                yield Source.Make( indent + 2, "None" )
                
            yield Source.Empty                
                
        }
        
    let Clone (schema:ISchema) (currentNamespace:string) (var:string) (ft:FieldType) (options:Options) =
        
        let rec impl (var:string) (ft:FieldType) =
            match ft with
            | FieldType.Simple(name,isOptional) ->
                if schema.IsScalar name || schema.IsEnum name  then  
                    if name.Equals("serialisable",System.StringComparison.OrdinalIgnoreCase) then
                        sprintf "match %s with | :? System.ICloneable as ic -> ic.Clone() :?> ITypeSerialisable | _ -> failwithf \"Not cloneable!\"" var
                    else
                        var
                else
                    if isOptional then
                        sprintf "%s |> Option.map ( fun v -> %s )" var (impl "v" (FieldType.Simple(name,false)))
                    else
                        if schema.IsRecord name || schema.IsUnion name then
                            sprintf "%s.Clone()" var 
                        else if schema.IsInterface name then
                            let castAs = NameToFSharpType schema options (Some currentNamespace) name
                            sprintf "%s.Clone() :?> %s" var castAs
                        else
                            var
                            
            | FieldType.Array(inner,isOptional) ->
                if isOptional then
                    sprintf "%s |> Option.map ( fun v -> %s )" var (impl "v" (FieldType.Array(inner,false)))
                else
                    let useArrayCopy =
                        match inner with 
                        | FieldType.Simple(name,_) -> schema.IsScalar name || schema.IsEnum name
                        | _ -> false
                                
                    if useArrayCopy then 
                        sprintf "Array.copy %s" var
                    else
                        sprintf "%s |> Array.map ( fun v -> %s )" var (impl "v" inner)

            | FieldType.Set(inner,isOptional) ->
                if isOptional then
                    sprintf "%s |> Option.map ( fun v -> %s )" var (impl "v" (FieldType.Set(inner,false)))
                else
                    sprintf "%s |> Set.map ( fun v -> %s )" var (impl "v" inner)
                                
            | FieldType.Map(inner,isOptional) ->
                if isOptional then
                    sprintf "%s |> Option.map ( fun v -> %s )" var (impl "v" (FieldType.Map(inner,false)))
                else
                    sprintf "%s |> Map.map ( fun k v -> %s )" var (impl "v" inner)
                
        //(impl (sprintf "this.%s" field.Name) field.FieldType)
        impl var ft
