namespace Example.Types.Formatters.FSharp

open Example.Types.Schema

open Example.Types.Formatters
open Example.Types.Formatters.FSharp
open Example.Types.Formatters.FSharp.Helpers

module Interface =
    
    let Format (schema:ISchema) (options:Options) (iface:Interface) =
        
        let nameSpace =
            Helpers.NamespaceFromName options.Namespace iface.Name
            
        let shortName =
            Helpers.ShortName iface.Name
            
        let interfaceEnd =
            if iface.Fields.Length = 0 && not(options.Cloneable) then  "interface end" else ""
            
        let lines =
            seq {
                yield Source.Make( 0, sprintf "namespace %s" nameSpace )
                
                yield Source.Empty
                
                yield Source.Make( 0, sprintf "type %s = %s" shortName interfaceEnd )
                
                if options.Cloneable then
                    yield Source.Make( 1, sprintf "inherit System.ICloneable" )
                    
                yield!
                    iface.Fields |> Seq.mapi ( fun idx field ->
                        Source.Make( 1, sprintf "abstract %s : %s with get" field.Name (Helpers.FieldTypeToFSharpType schema options nameSpace field.FieldType) ) )
            }
            
        let text =
            lines |> Seq.map Helpers.SourceToText |> String.concat "\n"
            
        let result =
            
            let name =
                sprintf "%s.fs" iface.Name
                
            Format.Make( "text", name, text |> System.Text.Encoding.UTF8.GetBytes )
        
        result |> Array.singleton

