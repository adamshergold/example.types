namespace Example.Types.Formatters.FSharp

open Example.Types.Formatters

[<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]
type Options = {
    Namespace : string
    Serdes : Serde[]
    Filters : string[] option
    OpenNamespaces : Set<string>
    AdditionalScalarToFSharpTypes : Map<string,string*string option>
    Cloneable : bool
}
with 
    static member Make( ns:string, serdes, filters, opens, scalarToFSharpTypes, cloneable ) =
        {
            Namespace = ns.Trim()
            Serdes = serdes
            Filters = filters
            OpenNamespaces = opens
            AdditionalScalarToFSharpTypes = scalarToFSharpTypes
            Cloneable = cloneable
        }
        
    static member Default = {
        Namespace = "Example"
        Serdes = [| Serde.JSON; Serde.Binary |]
        Filters = None
        OpenNamespaces =
            Set.empty
        AdditionalScalarToFSharpTypes =
            Map.ofSeq [
                "Serialisable",  ( "ITypeSerialisable", None)
                "LocalDateTime", ( "LocalDateTime", Some "NodaTime")
                "LocalDate",     ( "LocalDate", Some "NodaTime")
                "ZonedDateTime", ( "ZonedDateTime", Some "NodaTime")
            ]
        Cloneable = true
    }