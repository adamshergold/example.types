namespace Example.Types.Formatters.FSharp

open Example.Types.Schema
open Example.Types.Schema.Extensions

open Example.Types.Formatters
open Example.Types.Formatters.FSharp
open Example.Types.Formatters.FSharp.Helpers

module Union =
    
    let Format (schema:ISchema) (options:Options) (union:Union) =
        
        let nameSpace =
            Helpers.NamespaceFromName options.Namespace union.Name
            
        let shortName =
            Helpers.ShortName union.Name
            
        let hasSerialisation =
            options.Serdes.Length > 0
            
        let namespacesToOpen =
            Helpers.NamespacesToOpen options
            
        let lines =
            seq {
                yield Source.Make( 0, sprintf "namespace %s" nameSpace )
                
                yield Source.Empty
                
                if namespacesToOpen.Length > 0 then 
                    yield!
                        namespacesToOpen |> Seq.map ( fun ns -> Source.Make( 0, sprintf "open %s" ns ) )
                
                    yield Source.Empty
                
                yield Source.Make( 0, sprintf "type %s =" shortName )
                
                yield!
                    union.Cases
                    |> Seq.map ( fun field ->
                        let fst = Helpers.FieldTypeToFSharpType schema options nameSpace field.FieldType
                        Source.Make( 1, sprintf "| %s of %s" field.Name fst ) )
                    
                if hasSerialisation || options.Cloneable then
                    yield Source.Make( 0, "with" )
                
                if options.Cloneable then
                    yield Source.Make( 1, "member this.Clone () =" )
                    yield Source.Make( 2, "let _result =" )
                    yield Source.Make( 3, "match this with" )
                    
                    yield!
                        union.Cases
                        |> Seq.map ( fun case ->
                            let clone = Helpers.Clone schema nameSpace "v" case.FieldType options 
                            Source.Make( 3, sprintf "| %s( v ) -> %s( %s )" case.Name case.Name clone) )
                        
                    yield Source.Make( 2, "_result" )
                        
                    yield Source.Empty
                    
                    yield Source.Make( 1, "interface System.ICloneable")
                    yield Source.Make( 2, "with" )
                    yield Source.Make( 3, "member this.Clone () =" )
                    yield Source.Make( 4, "box <| this.Clone()" )
                    yield Source.Empty
                    
                if hasSerialisation then
                    
                    
                    yield Source.Make( 1, sprintf "interface ITypeSerialisable" )
                    yield Source.Empty
                    
                    yield Source.Make( 0, sprintf "module private %s_Serdes =" shortName )
                    yield Source.Empty
                    
                    if HasSerde options Serde.Binary then
                        
                        yield Source.Make( 1, sprintf "let %s_Serde =" (Helpers.SerdeName Serde.Binary) )
                        
                        yield Source.Make( 2, sprintf "{ new ITypeSerde<%s>" shortName )
                        yield Source.Make( 3, sprintf "with" )
                        
                        yield Source.Make( 4, sprintf "member this.TypeName =" )
                        yield Source.Make( 5, sprintf "\"%s\"" union.Name )
                        yield Source.Empty

                        yield Source.Make( 4, sprintf "member this.ContentType =" )
                        yield Source.Make( 5, sprintf "\"binary\"" )
                        yield Source.Empty

                        yield Source.Make( 4, sprintf "member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =" )
                        yield Source.Empty
                        yield Source.Make( 5, "use bs =")
                        yield Source.Make( 6, "BinarySerialiser.Make( serde, stream, this.TypeName )")
                        yield Source.Empty
                        yield Source.Make( 5, "match v with" )
                        
                        yield!
                            union.Cases
                            |> Seq.map ( fun field ->
                                seq {
                                    yield Source.Make( 5, sprintf "| %s( v ) ->" field.Name )
                                    yield Source.Make( 6, sprintf "bs.Write( \"%s\" )" field.Name )
                                    
                                    yield!
                                        Helpers.BinaryFieldSerialiser schema 6 options field (Some "v")

                                } )
                            |> Seq.concat
                        
                        yield Source.Make( 4, sprintf "member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =" )
                        yield Source.Empty
                        yield Source.Make( 5, "use bds =")
                        yield Source.Make( 6, "BinaryDeserialiser.Make( serde, stream, this.TypeName )")
                        yield Source.Empty
                        
                        yield Source.Make( 5, "let result =" )
                        yield Source.Make( 6, "match bds.ReadString() with" )
                        
                        yield!
                            union.Cases
                            |> Seq.map ( fun field ->
                                seq {
                                    yield Source.Make( 6, sprintf "| _ as v when v.Equals( \"%s\", System.StringComparison.OrdinalIgnoreCase ) ->" field.Name)
                                    yield Source.Empty
                                    yield!
                                        Helpers.BinaryFieldDeserialiser schema 7 options nameSpace field (Some "_v")
                                    yield Source.Make( 7, sprintf "%s.%s( _v )" shortName field.Name )
                                    yield Source.Empty
                                } )
                            |> Seq.concat

                        yield Source.Make( 6, "| _ as v ->" )
                        yield Source.Make( 7, sprintf "failwithf \"Unexpected union case seen when deserialising %s: '%%s'\" v" shortName )
                        yield Source.Empty
                        
                        yield Source.Make( 5, "result }")
                        yield Source.Empty
                        
                    if HasSerde options Serde.JSON then
                         
                        yield Source.Make( 1, sprintf "let %s_Serde =" (Helpers.SerdeName Serde.JSON) )
                        
                        yield Source.Make( 2, sprintf "{ new ITypeSerde<%s>" shortName )
                        yield Source.Make( 3, sprintf "with" )
                        
                        yield Source.Make( 4, sprintf "member this.TypeName =" )
                        yield Source.Make( 5, sprintf "\"%s\"" union.Name )
                        yield Source.Empty

                        yield Source.Make( 4, sprintf "member this.ContentType =" )
                        yield Source.Make( 5, sprintf "\"json\"" )
                        yield Source.Empty

                        yield Source.Make( 4, sprintf "member this.Serialise (serde:ISerde) (stream:ISerdeStream) v =" )
                        yield Source.Empty
                        yield Source.Make( 5, "use js =")
                        yield Source.Make( 6, "JsonSerialiser.Make( serde, stream, this.ContentType )")
                        yield Source.Empty
                        
                        yield Source.Make( 5, "js.WriteStartObject()")
                        yield Source.Make( 5, "js.WriteProperty serde.Options.TypeProperty" )
                        yield Source.Make( 5, "js.WriteValue this.TypeName" )
                        yield Source.Empty
                        
                        yield Source.Make( 5, "match v with" )
                        
                        yield!
                            union.Cases
                            |> Seq.map ( fun field ->
                                seq {
                                    yield Source.Make( 5, sprintf "| %s( v ) ->" field.Name )
                                    
                                    yield!
                                        Helpers.JsonFieldSerialiser schema 6 options field (Some "v")
                                } )
                            |> Seq.concat
                        
                        yield Source.Make( 5, "js.WriteEndObject()" )
                        yield Source.Empty
                        
                        yield Source.Make( 4, sprintf "member this.Deserialise (serde:ISerde) (stream:ISerdeStream) =" )
                        yield Source.Empty
                        yield Source.Make( 5, "use jds =")
                        yield Source.Make( 6, "JsonDeserialiser.Make( serde, stream, this.ContentType, this.TypeName )")
                        yield Source.Empty
                        
                        yield!
                            union.Cases
                            |> Seq.map ( fun field ->
                                Helpers.JsonFieldOnHandler schema 5 options nameSpace field ) 
                            |> Seq.concat
                            
                        yield Source.Empty
                        
                        yield Source.Make( 5, "jds.Deserialise()" )
                        yield Source.Empty
                        
                        yield Source.Make( 5, "let result =" )
                        
                        yield!
                            union.Cases
                            |> Seq.mapi ( fun idx field ->
                                seq {
                                    let prefix =
                                        if idx = 0 then "if" else "else if"
                                    
                                    yield Source.Make( 6, sprintf "%s jds.Handlers.Has \"%s\" then" prefix field.Name )
                                    yield Source.Make( 7, sprintf "%s.%s( jds.Handlers.TryItem<_>( \"%s\" ).Value )" shortName field.Name field.Name )
                                    })
                            |> Seq.concat

                        yield Source.Make( 6, "else" )
                        yield Source.Make( 7, sprintf "failwithf \"Unable to determine union case when deserialising [%%s]\" this.TypeName" )
                        yield Source.Empty
                        
                        yield Source.Make( 5, "result }" )
                        yield Source.Empty
                        
                    if hasSerialisation then
                        yield Source.Make( 0, sprintf "type %s with" shortName )
                        yield Source.Empty
                    
                        yield! 
                            options.Serdes
                            |> Seq.sortBy ( fun s -> s.ToString() )
                            |> Seq.map ( fun serde ->
                                let serdeName = Helpers.SerdeName serde
                                seq {
                                    yield Source.Make( 1, sprintf "static member %s_Serde = %s_Serdes.%s_Serde" serdeName shortName serdeName )
                                    yield Source.Empty
                                } )
                            |> Seq.concat
                        
            }
            
        let text =
            lines |> Seq.map Helpers.SourceToText |> String.concat "\n"
            
        let result =
            
            let name =
                sprintf "%s.fs" union.Name
                
            Format.Make( "text", name, text |> System.Text.Encoding.UTF8.GetBytes )
        
        result |> Array.singleton

