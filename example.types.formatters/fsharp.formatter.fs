namespace Example.Types.Formatters.FSharp

open Example.Types.Schema
open Example.Types.Formatters

type Formatter( options: Options ) =
    
    static member Make( options ) =
        new Formatter( options ) :> IFormatter
        
    member this.Format (schema:ISchema) (name:string) =
        
        match schema.TryLookup name with
        | Some definition ->
            match definition.Element with
            | Element.Enum(e) -> Enum.Format options e
            | Element.Interface(i) -> Interface.Format schema options i
            | Element.Record(r) -> Record.Format schema options r
            | Element.Union(r) -> Union.Format schema options r
            | _ -> Array.empty
        | None ->
            failwithf "Unable for format '%s' as no entry found in Schema" name
        
    interface IFormatter
        with
            member this.Format (schema:ISchema) (name:string) =
                this.Format schema name
        