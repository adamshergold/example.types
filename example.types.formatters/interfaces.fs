namespace Example.Types.Formatters

open Example.Types.Schema

type Serde = 
    | JSON
    | Binary 
with
    override this.ToString () = 
         match this with 
         | JSON -> "JSON"
         | Binary -> "Binary"
        
    static member Parse (s:string) = 
        match s.ToLower() with 
        | _ as v when v = "json" -> JSON
        | _ as v when v = "binary" -> Binary 
        | _ -> failwithf "Unable to parse [%s] as Serialiser" s
            
type Format = {
    ContentType : string
    Name : string
    Body : byte[]
}
with
    static member Make( ct, name, body ) =
        { ContentType = ct; Name = name; Body = body }
        
type IFormatter =
    abstract Format : ISchema -> string -> Format[]
    
