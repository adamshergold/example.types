namespace Example.Types.Schema.Tests

open Xunit
open Xunit.Abstractions 

open FParsec

open Example.Types.Schema
open Example.Types.Schema.Extensions

type ExtensionsShould( testOutputHelper:ITestOutputHelper) =

    member val TestOutputHelper = testOutputHelper
    
    static member ParseFieldTestCases
        with get () =
            seq {
                yield [|
                    box <| "Name : String" 
                    box <| Field.Make( "Name", FieldType.Simple( "String" , true ) )
                |]    

                yield [|
                    box <| "Name : String!" 
                    box <| Field.Make( "Name", FieldType.Simple( "String", false ) )
                |]    
                                
                yield [|
                    box <| "Name : [String!]" 
                    box <| Field.Make( "Name", FieldType.Array( FieldType.Simple( "String" , false ), true ) ) 
                |]
                
                yield [|
                    box <| "Name : [String!]!" 
                    box <| Field.Make( "Name", FieldType.Array( FieldType.Simple( "String", false ), false ) )
                |]
                
                yield [|
                    box <| "Name : {String}" 
                    box <| Field.Make( "Name", FieldType.Set( FieldType.Simple(  "String", true ), true ) )
                |]    

                yield [|
                    box <| "Name : <String!>!" 
                    box <| Field.Make( "Name", FieldType.Map( FieldType.Simple(  "String", false ), false ) )
                |]    
                            
            }

    static member ParseEnumTestCases
        with get () =
            seq {
                
                yield [|
                    box <| "enum Example.Colour { RED GREEN BLUE }" 
                    box <| Enum.Make( "Example.Colour", [| "RED"; "GREEN"; "BLUE" |] )
                |]    
                            
            }
                
    static member ParseInterfaceTestCases
        with get () =
            seq {
                yield [|
                    box <| "interface IPerson { Name : String! }"  
                    box <| Interface.Make( "IPerson",
                                        [|
                                            Field.Make( "Name",
                                                FieldType.Simple( "String", false ) )
                                        |] )
                |]    
            }

    static member ParseUnionTestCases
        with get () =
            seq {
                yield [|
                    box <| "union Status = Married : Married! | Single : Single!"  
                    box <| Union.Make( "Status",
                                       [|
                                          Field.Make( "Married", FieldType.Simple( "Married", false ) )
                                          Field.Make( "Single", FieldType.Simple( "Single", false ) )
                                       |] )
                |]
                
                yield [|
                    box <| "union Status = Married : [Married!]!"  
                    box <| Union.Make( "Status",
                                        [|
                                            Field.Make( "Married",
                                                        FieldType.Array( FieldType.Simple( "Married", false ), false ) ) 
                                        |] )
                |]    
                
            }
                    
    static member ParseRecordTestCases
        with get () =
            seq {
                yield [|
                    box <| "type Person implements IPerson { Name : String! Address: [String!] }" 
                    box <| Record.Make( "Person",
                                        [|
                                            Field.Make( "Name",
                                                FieldType.Simple( "String", false ) )
                                            
                                            Field.Make( "Address",
                                                FieldType.Array( FieldType.Simple( "String", false ), true ) )
                                        |],
                                        [|
                                            "IPerson"
                                        |] )
                |]    
            }
            
    [<Fact>]
    member this.``Parse-Scalar`` () =
        let result = Helpers.ApplyParser "scalar Int64" Scalar.Parser
        let expected = Scalar.Make( "Int64" ) 
        Assert.Equal( expected, result )

    [<Theory>]
    [<MemberData("ParseEnumTestCases")>]
    member this.``Parse-Enum`` (source:string,expected:Enum) =
        let result = Helpers.ApplyParser source Enum.Parser
        Assert.Equal( expected, result )

    [<Theory>]
    [<MemberData("ParseInterfaceTestCases")>]
    member this.``Parse-Enum`` (source:string,expected:Interface) =
        let result = Helpers.ApplyParser source Interface.Parser 
        Assert.Equal( expected, result )

    [<Theory>]
    [<MemberData("ParseUnionTestCases")>]
    member this.``Parse-Union`` (source:string,expected:Union) =
        let result = Helpers.ApplyParser source Union.Parser
        Assert.Equal( expected, result )
            
    [<Theory>]
    [<MemberData("ParseFieldTestCases")>]
    member this.``Parse-Field`` (source:string,expected:Field) =
        let result = Helpers.ApplyParser source Field.Parser
        Assert.Equal( expected, result )

    [<Theory>]
    [<MemberData("ParseRecordTestCases")>]
    member this.``Parse-Record`` (source:string,expected:Record) =
        let result = Helpers.ApplyParser source Record.Parser
        Assert.Equal( expected, result )
