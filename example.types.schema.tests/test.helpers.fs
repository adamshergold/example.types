namespace Example.Types.Schema.Tests

open Xunit
open Xunit.Abstractions 

open Example.Types.Schema

type HelpersShould( testOutputHelper:ITestOutputHelper) =

    member val TestOutputHelper = testOutputHelper

    [<Fact>]
    member this.``ReportTrueForIsParseSuccess`` () =
        let pr = ParseResult.Success(Array.empty)
        Assert.True( Helpers.IsParseSuccess pr )
   
    [<Fact>]
    member this.``ReportFalseForIsParseSuccess`` () =
        let pr = ParseResult.Failure( ParseFailure.Make( "", "" ))
        Assert.False( Helpers.IsParseSuccess pr )
        
