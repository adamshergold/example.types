namespace Example.Types.Schema.Tests

open Xunit
open Xunit.Abstractions 

open Example.Types.Schema.Graph

type GraphShould( testOutputHelper:ITestOutputHelper) =

    member val TestOutputHelper = testOutputHelper

    [<Fact>]
    member this.``SayThatEmptyGraphHasNoNodes`` () = 
        let sut = Simple.Make()
        Assert.Equal( 0, sut.Count() )


    [<Fact>]
    member this.``ReportTheCorrectNodes`` () = 
        let sut = Simple.Make()
        sut.Add("a")
        sut.Add("b")
        Assert.Equal(2, sut.Count() )
        Assert.Contains("a",sut.Nodes() )
        Assert.Contains("b",sut.Nodes() )
         
    [<Fact>]
    member this.``ReportTheCorrectEdges`` () = 
        let sut = Simple.Make()
        sut.Add("a","b")
        sut.Add("b","c")
        Assert.Equal(3, sut.Count() )
        Assert.Contains("a",sut.Nodes() )
        Assert.Contains("b",sut.Nodes() )
        Assert.Contains("c",sut.Nodes() )
        Assert.Contains( ("a","b") ,sut.Edges() )
        Assert.Contains( ("b","c") ,sut.Edges() )
         
    [<Fact>]
    member this.``ReportTheCorrectOrder`` () = 
        let sut = Simple.Make()
        sut.Add("a","b")
        sut.Add("b","c")
        let order = [| "c"; "b"; "a" |]
        Assert.Equal( order, sut.Sort() |> Array.toSeq )         
        
    [<Fact>]
    member this.``ReportTheChildren`` () = 
        let sut = Simple.Make()
        sut.Add("a","b")
        sut.Add("b","c")
        Assert.Equal( [| "c" |], sut.Children "b" |> Array.toSeq )
        Assert.Equal( [| "b" |], sut.Children "a" |> Array.toSeq )
