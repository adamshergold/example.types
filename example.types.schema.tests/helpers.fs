namespace Example.Types.Schema.Tests

open FParsec

open Example.Types.Schema

module Helpers =  

    let ApplyParser (s:string) (parser:ISchema->Parser<'T,unit>) =
        let schema = Schema.Make()
        match run (parser schema) s with 
        | ParserResult.Success(result,_,_) ->
            result
        | ParserResult.Failure(error,_,_) ->
            failwithf "Error! %s" error 

        
               

        
