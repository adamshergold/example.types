namespace Example.Types.Schema.Tests

open Microsoft.Extensions.Logging

open Xunit
open Xunit.Abstractions 

open Example.Types.Schema
open Example.Types.Schema.Extensions

type SchemaShould( testOutputHelper:ITestOutputHelper ) =

    let logger =
        Logging.CreateLogger testOutputHelper
        
    let sut () =
        
        let options =
            { SchemaOptions.Default with Logger = Some logger }
            
        Schema.Make( options )
        
    static member OrderTestCases
        with get () =
            seq {
                yield
                    [|
                        box <| "enum Colour {RED} type Person { Name:String! Colour: Colour}"
                        box <| [| "Colour"; "Person" |]
                    |]

                yield
                    [|
                        box <| "enum Colour {RED} type Person { Name:String! Colour: Colour Pet : IPet} interface IPet { Name : String! } type Cat implements IPet { Fluffy : Boolean! }"
                        box <| [| "Colour"; "IPet"; "Person"; "Cat" |]
                    |]
                                
            }
            
    [<Fact>]
    member this.``ReportEmptyWhenCreated`` () =
        let sut = sut()
        // Must have the built-ins
        Assert.True( sut.Count >= 4)
        Assert.True( sut.TryLookup "Int" |> Option.isSome )
        Assert.True( sut.TryLookup "String" |> Option.isSome )
        Assert.True( sut.TryLookup "Float" |> Option.isSome )
        Assert.True( sut.TryLookup "Boolean" |> Option.isSome )
         
    [<Fact>]
    member this.``ClearWorks`` () =
    
        let sut = sut()
        
        let originalCount = sut.Count
        
        // Must have the built-ins
        Assert.True( originalCount >= 4)
        sut.Clear()
        // we don't clear the builtins
        Assert.Equal( originalCount, sut.Count )
        
    [<Fact>]
    member this.``ParseValidDefinitionAndAllowLookup`` () =
        
        let sut = sut()
        
        let nItems = sut.Count
        
        let nAdded = ref 0
        let nRemoved = ref 0
        
        let onAdded args =
            System.Threading.Interlocked.Increment( nAdded ) |> ignore

        let onRemoved args =
            System.Threading.Interlocked.Increment( nRemoved ) |> ignore
                    
        sut.OnAddedOrUpdated.Add onAdded
        sut.OnRemoved.Add onRemoved
        
        let result =
            sut.Parse "type Person { Name : String! }"
        
        Assert.True( Helpers.IsParseSuccess result )
        
        let lookup =
            sut.TryLookup "Person"
            
        Assert.True( lookup.IsSome )
        Assert.Equal( "Person", lookup.Value.Element.Name )

        Assert.True( sut.Count = nItems + 1 )
        
        Assert.True( sut.Remove "Person" )
        
        Assert.Equal( 1, !nAdded )
        Assert.Equal( 1, !nRemoved )
        
    [<Theory>]
    [<MemberData("OrderTestCases")>]
    member this.``OrderShouldWorkCorrectly`` (text:string,expectedOrder:string[]) =
        
        let sut = sut()
        
        let result = sut.Parse text
        
        match result with
        | ParseResult.Failure(failure) -> logger.LogError( "{Error}", failure.Error )
        | _ -> ()
        
        Assert.True( Helpers.IsParseSuccess result )
        
        Assert.Equal( expectedOrder, sut.Order() |> Seq.ofArray )
        