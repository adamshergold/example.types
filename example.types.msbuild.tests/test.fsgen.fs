namespace Example.Types.MSBuild.Tests

open Xunit
open Xunit.Abstractions 

open Example.Types.MSBuild
open Example.Types.MSBuild.Verbs

type MSBuildShould( oh:ITestOutputHelper) =

    let logger = Logging.CreateLogger oh
    
    [<Fact>]
    member this.``DoFSGenOkay`` () =
        
        let sut = FSGenOptions.Make()
        
        sut.Source <- "../../../../example.types.test/definitions"
        sut.UpdateProject <- "../../../test.fsproj"
        
        let status = sut.Execute logger
        
        Assert.Equal( 0, status )

