namespace Example.Types.MSBuild.Tests

open Xunit.Abstractions 

open Microsoft.Extensions.Logging 

open Serilog

module Logging = 

    let CreateLogger (oh:ITestOutputHelper) =
     
        let config =
            
            let levelSwitch = 
                Serilog.Core.LoggingLevelSwitch() 
    
            levelSwitch.MinimumLevel <- Serilog.Events.LogEventLevel.Verbose
            
            let config =
                Serilog.LoggerConfiguration()
                    .MinimumLevel.ControlledBy(levelSwitch)
                    .WriteTo.TestOutput(
                        oh, 
                        Events.LogEventLevel.Debug,
                        outputTemplate = "{Timestamp:yyyy-MM-dd HH:mm:ss} [{Level}] {Message} {Properties}{NewLine}{Exception}" ) 
            config 
            

        let factory =
            
            let logger =
                config.CreateLogger()             
                
            let lf = 
                new LoggerFactory()
                    
            lf.AddSerilog(logger)
        
        factory.CreateLogger("test") 




