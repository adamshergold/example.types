﻿namespace Example.Types.MSBuild

open Microsoft.Extensions.Logging
open Microsoft.Extensions.Logging.Console
open Microsoft.Extensions.DependencyInjection

open CommandLine

open Example.Types.MSBuild.Verbs

module Main = 

    [<EntryPoint>]
    let main args =
    
        let result = 
            CommandLine.Parser.Default.ParseArguments<FSGenOptions,InfoOptions>( args )
        
        let status =
        
            match result with
            | :? Parsed<obj> as parsed ->
            
                let logLevel = 
                    match parsed.Value with
                    | :? CommonOptions as co -> co.Level
                    | _ -> "debug" 

                let logger =
          
                    let sc =
                        new ServiceCollection()
                    
                    let fn (builder:ILoggingBuilder) =
                        builder.AddConsole() |> ignore
                        ()
                        
                    let sa = System.Action<ILoggingBuilder>( fn )
                    
                    sc.AddLogging( sa ) |> ignore
                    
                    let lf =
                        sc.BuildServiceProvider().GetService<ILoggerFactory>()
                        
                    lf.CreateLogger("cli")
                    
                match parsed.Value with
                | :? InfoOptions as opts ->
                    opts.Execute( logger )
                | :? FSGenOptions as opts ->
                    opts.Execute( logger )
                | _ ->
                    logger.LogCritical( "Parsed command line but failed to match with any options!" )
                    -1
                    
            | :? NotParsed<obj> as np ->
                let msg = np.Errors |> Seq.map ( fun err -> err.ToString() ) |> String.concat ";"
                failwithf "Failed to parse command line options! [%s]" msg
                
            | _ ->
                failwithf "Failed to parse command line options!" 
                -1

        status 