#/bin/sh -x

dotnet run --no-build --no-restore  -- fsgen --level Debug --namespace Klarity.Types.Serialisation.Test --mode "generate(JSON)" --source ../klarity.types.serialisation.test --target generated $*
