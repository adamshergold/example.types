namespace Example.Types.MSBuild

open CommandLine

[<System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute>]
type CommonOptions() = 
    let mutable _level = "info"
with
    [<Option("level",Required=false, HelpText="Logging level: verbose, debug, info (default), warn or error")>]
    member this.Level with get () = _level and set newV = _level <- newV  