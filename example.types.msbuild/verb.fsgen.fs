namespace Example.Types.MSBuild.Verbs

open System.Xml 

open CommandLine

open Microsoft.Extensions.Logging

open FParsec

open Example.Types.MSBuild

open Example.Types.Schema
open Example.Types.Schema.Extensions
open Example.Types.Formatters

//open Example.Types.Tools
//open Example.Types.Tools.Interfaces
//open Example.Types.Tools.Formatting
//open Example.Types.Tools.Formatting.FSharp

[<AutoOpen>]
module FSGenImpl = 

    type Mode = 
        | Skip 
        | ParseOnly 
        | Generate of Serde[] 
    with 
        override this.ToString () = 
            match this with 
            | Skip -> "skip"
            | ParseOnly -> "parseOnly" 
            | Generate(serialisers) -> sprintf "generate(%s)" (serialisers |> Seq.map (fun s -> s.ToString() ) |> String.concat ";")
            
        static member Parse (s:string) =
            match s.ToLower() with 
            | _ as v when v = "skip" -> Skip
            | _ as v when v = "parseOnly" -> ParseOnly
            | _ as v when v.StartsWith( "generate(" ) ->
                let serialisers = 
                    v.Substring( 9, v.Length - 9 - 1 ).Split( [| ';' |] )
                    |> Seq.map ( fun v -> Serde.Parse(v) )
                    |> Array.ofSeq
                Generate(serialisers)
            | _ as v ->
                failwithf "Unable to parse [%s] as mode" s      
                         
                
[<Verb("fsgen")>]
type FSGenOptions() =
    inherit CommonOptions() 
    
    let mutable _source : string = "definitions" 
    let mutable _target : string = ".generated"
    let mutable _namespace : string = ""
    let mutable _mode : string = Mode.Generate( [| Serde.JSON; Serde.Binary |] ).ToString()
    let mutable _updateProject : string = ""
    let mutable _openNamespaces : string = ""
    
with    
    static member Make () = 
        new FSGenOptions() 

    override this.ToString () = 
        sprintf "FsgOptions(Source=%s,Target=%s,Namespace=%s,Mode=%s,UpdateProject=%s,OpenNamespaces=%s)" this.Source this.Target this.Namespace this.Mode this.UpdateProject this.OpenNamespaces
        
    [<Option("source", Required = false, HelpText = "Folder containing type definitions (default is 'definitions') ")>]
    member this.Source with get () = _source and set newV = _source <- newV 

    [<Option("mode", Required = false, HelpText = "skip (do nothing), parse (no code gen), generate([serdes])) (where serdes is one of JSON, Binary)")>]
    member this.Mode with get () = _mode and set newV = _mode <- newV 

    [<Option("target", Required = false, HelpText = "Output folder for generated code (default is '.generated')")>]
    member this.Target with get () = _target and set newV = _target <- newV 

    [<Option("namespace", Required = false, HelpText = "Namespace into which types are embedded (default is nothing i.e. namespace will start with type-name)")>]
    member this.Namespace with get () = _namespace and set newV = _namespace <- newV 

    [<Option("updateProject", Required = false, HelpText = "Name of .fsproj file that will be updated with compile includes for generated .fs files")>]
    member this.UpdateProject with get () = _updateProject and set newV = _updateProject <- newV 

    [<Option("openNamespaces", Required = false, HelpText = "Any additional 'open' commands at top of generated code (comma separated list)")>]
    member this.OpenNamespaces with get () = _openNamespaces and set newV = _openNamespaces <- newV 
    
    let updateFsProj (projectFile:string) (target:string) (manifest:seq<string>) = 
    
        let doc = new XmlDocument()
        
        doc.Load( projectFile )

        let processItemGroup(node:XmlNode) =
        
            let includes = 
                node.ChildNodes
                |> Seq.cast<XmlNode>
                |> Seq.filter ( fun child -> child.Name = "Compile" )
                |> Array.ofSeq
                
            includes 
            |> Seq.iter ( fun child ->
                node.RemoveChild child |> ignore )
                
            (node.ChildNodes.Count = 0)
            
        let processProject(project:XmlNode) =
        
            let children =
                project.ChildNodes
                |> Seq.cast<XmlNode>
                |> Array.ofSeq
                
            children    
            |> Seq.iter ( fun child ->   
                if child.Name = "ItemGroup" then  
                    if processItemGroup(child) then
                        project.RemoveChild child |> ignore ) 
            
            let itemGroup = 
                doc.CreateNode( XmlNodeType.Element, "ItemGroup", null )
                
            manifest |> Seq.iter ( fun inc ->
                let child = doc.CreateNode( XmlNodeType.Element, "Compile", null )
                let attr = doc.CreateAttribute("Include")
                attr.Value <- inc
                child.Attributes.Append(attr) |> ignore
                itemGroup.AppendChild child |> ignore )
                                                
            project.AppendChild itemGroup |> ignore                    
                                
                    
        doc.ChildNodes
        |> Seq.cast<XmlNode>
        |> Seq.iter ( fun child ->
            if child.Name = "Project" then 
                processProject(child) )
                
        doc.Save( projectFile ) 

        
    member this.Execute (logger:ILogger) =

        logger.LogInformation( "Execute with options {Options}", this )

        let mode = 
            Mode.Parse( this.Mode ) 
            
        let isSkip = 
            match mode with | Skip -> true | _ -> false 

        let isCodeGenerate, serdesToGenerate = 
            match mode with | Generate(serdes) -> true, serdes | _ -> false, Array.empty 
                            
        let md5 (input:byte[]) =
         
            use ms = 
                new System.IO.MemoryStream(input)
            
            use md5 = 
                System.Security.Cryptography.MD5.Create()
        
            System.BitConverter.ToString(md5.ComputeHash(ms))

        if isSkip then 
            0
        else
            try
                logger.LogDebug( "Checking target directory {Target}", this.Target )
                
                if not <| System.IO.Directory.Exists( this.Target ) then
                    System.IO.Directory.CreateDirectory( this.Target ) |> ignore 
                    
                logger.LogInformation( "Scanning source folder {Source}", this.Source )
                    
                let files = 
                    if System.IO.Directory.Exists( this.Source ) then
                        System.IO.Directory.EnumerateFiles( this.Source, "*.def", System.IO.SearchOption.AllDirectories ) |> Array.ofSeq
                        
                    elif System.IO.File.Exists( this.Source ) then
                        this.Source |> Array.singleton
                    else
                        let cwd = System.IO.Directory.GetCurrentDirectory()
                        failwithf "Specified source (%s) was neither a folder nor a file! (cwd is '%s')" this.Source cwd
    
                logger.LogDebug( "Found {Files}", files.Length )
    
                let schema =
                    
                    let options =
                        { SchemaOptions.Default with Logger = Some logger }
                        
                    Schema.Make( options )
                    
                files                
                |> Array.iter ( fun file ->
                
                    logger.LogInformation( "Parsing {File}", file )
                
                    let text = 
                        System.IO.File.ReadAllText( file )
                    
                    match schema.Parse text with
                    | Success(_) ->
                        () 
                    | Failure(pf) ->
                        logger.LogError( "Parse failure for {Source} with [{Error}]", pf.Source, pf.Error ) 
                        failwithf "Parse failure for [%s] with [%s]" pf.Source pf.Error )
                        
                let formatter =
             
                    let serdes =
                        serdesToGenerate
                        
                    let options =
                        
                        let openNamespaces =
                            if this.OpenNamespaces.Length > 0 then this.OpenNamespaces.Split( ',' ) |> Set.ofSeq else Set.empty
                            
                        { FSharp.Options.Default
                              with
                                  Namespace = this.Namespace
                                  Serdes = serdes
                                  Filters = None
                                  OpenNamespaces = openNamespaces }
                    
                    FSharp.Formatter.Make( options )
    
                let nFilesWritten = ref 0 
                
                let filesByType =                   
                    schema.Items
                    |> Seq.map ( fun definition ->
                        
                        let formats = 
                            formatter.Format schema definition.Element.Name 
                        
                        definition.Element.Name, formats )
                    |> Map.ofSeq 
                    
                let expectedFiles =                     
                    filesByType
                    |> Map.toSeq
                    |> Seq.map ( fun (name,formats) ->
                    
                        formats
                        |> Seq.filter ( fun format -> format.ContentType = "text" )
                        |> Seq.map ( fun format ->
                        
                            let targetFile = 
                                System.IO.Path.Combine( this.Target, format.Name )
                                
                            let needToSave = 
                                if not <| System.IO.File.Exists( targetFile ) then
                                    logger.LogDebug( "Target file {Target} does not exist", targetFile )   
                                    true
                                else 
                                    let generatedMD5 = 
                                        format.Body |> md5
                                        
                                    let existingLines = 
                                        System.IO.File.ReadAllBytes( targetFile )
                                                                            
                                    let existingMD5 = 
                                        existingLines |> md5
                                        
                                    logger.LogDebug( "Checksums: {Existing} vs {Generated}", existingMD5, generatedMD5 )                             
                                    generatedMD5 <> existingMD5         
                        
                            if needToSave then
                                if isCodeGenerate then    
                                    logger.LogInformation( "Code generating {Name}/{File} and writing to {TargetFile}/{Length}", name, format.Name, targetFile, format.Body.Length )
                                    System.IO.File.WriteAllBytes( targetFile, format.Body ) 
                                    System.Threading.Interlocked.Increment(nFilesWritten) |> ignore
                            else
                                logger.LogInformation( "Code generating {Name}/{File} but NOT rewriting {Generated} as unchanged!", name, format.Name, targetFile )
                                
                            targetFile ) )
                    |> Seq.concat
                    |> Set.ofSeq
                    
                let orderedTypes =
                    schema.Order()
                    
                let orderedFiles = 
                    orderedTypes
                    |> Seq.map ( fun t ->
                        filesByType.Item( t )
                        |> Array.map ( fun tf -> 
                            System.IO.Path.Combine( this.Target, tf.Name ) ) )
                    |> Seq.concat
                        
                System.IO.Directory.EnumerateFiles( this.Target )
                |> Seq.iter ( fun file ->
                    if not <| expectedFiles.Contains file then 
                        logger.LogInformation( "Removing unexpected file {File}", file )
                        System.Threading.Interlocked.Increment(nFilesWritten) |> ignore              
                        System.IO.File.Delete( file )
                        
                    () )
                                
                if this.UpdateProject.Length > 0 && System.IO.File.Exists( this.UpdateProject ) then 
    
                    //if !nFilesWritten > 0 then
                    if true then              
        
                        logger.LogInformation( "Updating {ProjectFile}", this.UpdateProject )
                                               
                        updateFsProj this.UpdateProject this.Target orderedFiles
                    else
                        logger.LogInformation( "Skipping update {ProjectFile} - no files written", this.UpdateProject )                                                                                                    
                                                                                    
                0 
            with
            | _ as ex ->
                logger.LogError( "{ErrorMessage}", ex.Message, ex )
                -1

