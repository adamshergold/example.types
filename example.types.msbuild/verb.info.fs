namespace Example.Types.MSBuild.Verbs

open CommandLine

open Microsoft.Extensions.Logging 

open Example.Types.MSBuild 

[<Verb("info")>]
type InfoOptions() =
    inherit CommonOptions() 
    
with    
    static member Make () = 
        new InfoOptions() 

    override this.ToString () = 
        sprintf "InfoOptions()"
        
    member this.Execute (logger:ILogger) =
    
        let assy = 
            System.Reflection.Assembly.GetExecutingAssembly()
        
        logger.LogInformation("Runtime Version {ImageRuntimeVersion}",assy.ImageRuntimeVersion)
        
        0
      
